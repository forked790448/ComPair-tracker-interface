
from . import banner,adc,master, style
import dash_html_components as html
import dash_core_components as dcc
import dash_bootstrap_components as dbc

def build_layer_live_panel():
    return html.Div(
        id="layer-live-panel-delme",
        className="layer-panel",
        children=[
            html.H5(children="Layer Status: "),
            """html.Span(id="l0-led", className="delme3", children=["0"]),
            html.Span(id="l1-led", className="delme3", children=["1"]),
            html.Span(id="l2-led", className="delme3", children=["2"]),
            html.Span(id="l3-led", className="delme3", children=["3"]),
            html.Span(id="l4-led", className="delme3", children=["4"]),
            html.Span(id="l5-led", className="delme3", children=["5"]),
            html.Span(id="l6-led", className="delme3", children=["6"]),
            html.Span(id="l7-led", className="delme3", children=["7"]),
            html.Span(id="l8-led", className="delme3", children=["8"]),
            html.Span(id="l9-led", className="delme3", children=["9"]),"""
        ]

    )


def build_main_tab():
    return html.Div(
        children=[
            dbc.CardHeader(
                dbc.Tabs(
                    id='top-level-tabs',
                    className='nav-justified boot-top-level-tabs',
                    children=[
                        dbc.Tab(label='Main', **style.top_level_tab_styling),
                        dbc.Tab(label='Layer 0-2',**style.top_level_tab_styling), # This will be for 0-4 eventually
                        dbc.Tab(label='Layer 3-6',**style.top_level_tab_styling) # This will be for 5-10 eventually
                    #dbc.Tab(id='rates-tab', children=rates.build_rate_card(), label="Rates", **style.tab_styling),
                    #dbc.Tab(id='ADC-tab', children=adc.build_adc_card(), label="ADC Data", **style.tab_styling),
                    #dbc.Tab(id='slowctl-tab', children=slowctl.build_slow_ctl_tab(), label="Slow Control",
                    #        **style.tab_styling),
                    ],
                ),
            )
        ]
    )