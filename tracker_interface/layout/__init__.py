""" Scripts and modules representing the GUI objects of the application.

    This package consists of various modules for the layout of the GUI application.
    This is built using the dash core components, dash device components and various html components.
    Each element and objects are within another object and so on. (Similar to volumes in GEANT4, geometry)
    The assets folder has .css files that define the styling of most of the objects in the layout.

    Note
    ----
    The html components (all components in general) are for dash therefore the syntax for the usage of
    these elements are slightly different than regular html programming. This is further extended to the
    css file representing the style of these components. Refer to the various examples or the dash
    document for a specific syntax.
"""
from .banner import *
from .adc import * 
from .master import * 
from .rates import *
from .style import * 
from .slowctl import * 