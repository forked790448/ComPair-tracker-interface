""" The layout module for the Slow Control Tab.
"""
# ComPair tracker quicklook layout
# Author: Sean Griffin
#
# This module contains layout information for the ComPair tracker. 
# Based heavily on the example given here: https://dash-gallery.plotly.host/dash-manufacture-spc-dashboard/
# July 13, 2021      Sambid K. Wasti
#                   Added extensive comments and doc-strings.

import dash_html_components as html
import dash_core_components as dcc
import dash_bootstrap_components as dbc
import dash_daq as ddaq
import plotly.graph_objs as go
from . import section_banner #direct import?
from . import style

valid_asics    = [dict(label=f"{i:02d}", value=i) for i in range(12)]

def build_asci_cfg_card():
    """ Builds the Asic Config card. This occupies 2 width (columns out of 12)

    Returns
    -------
    asic_cfg_card : html.Div
        The Div element containing the Config settings.
    """
    return html.Div(
        id="asic_cfg_card", 
        #className=
        children=[
            section_banner("ASIC Configuration Information"),
            html.Br(),
            html.P("NOTE: This skeleton is for demonstration purposes only."),
            dbc.InputGroup(
                size="lg",
                children=[
                    dbc.InputGroupAddon("ASIC", addon_type="prepend"),
                    dbc.Select(id='query-asic-vata-in', options=valid_asics),
                    dbc.InputGroupAddon(
                        dbc.Button("Query", id="query-asic-cfg-button"),
                        addon_type="append",
                    ),
                ]
            ),            
            dbc.Table(
                id='asic-cfg-table',
                size="sm",
                children=[
                    html.Thead(html.Tr([html.Th("Field"), html.Th("Value")]))
                ]
            )
        ],
    )


def build_slow_ctl_tab():
    """ Main build function for the Slow Control Tab.

        This is structured in Table and rows (12 columns). Currently, there are 2 main components, the
        Fee-stuffs (width = 3) and ASIC-config stuffs (width =2).

    Returns
    -------
    slow-ctl-panel-container: html.Div
        The whole slow control tab component.
    """
    return html.Div(
        id="slow-ctl-panel-container",
        className="twelve columns",
        children=[     
            dbc.Row(
                children=[        
                    dbc.Col(
                        width=3, 
                        children=[
                            section_banner("FEE Control"),
                            #html.Br(),
                            dbc.CardBody(
                                style={'textAlign': 'center'},
                                children=[
                                    dbc.ButtonGroup(
                                        style={'width' : '100%'},
                                        children=[
                                            dbc.Button("Enable",  id='fee-enable-button', color="success", size="lg"),   
                                            dbc.Button("Disable", id='fee-disable-button', color="danger", size="lg"),   
                                            dbc.Button("Query",   id='fee-query-button', color="primary", size="lg"),
                                        ]
                                    ),

                                    html.H4("FEE status:"),
                                    html.H4(id='fee-status-field'),
                                ],
                            ),
                        ]
                    ),

                    dbc.Col(
                        width=2, 
                        children=[
                            build_asci_cfg_card(),
                        ]
                    ),                    
                ]
            ),
            html.Hr(),
            dbc.Row(
                children=[        
                    html.Br(),
                    dbc.Col(
                        width=3, 
                        children=[
                            section_banner("Another Column")
                        ]
                    ),
                    dbc.Col(
                        width=7, 
                        children=[
                            section_banner("This one exists, also."),
                        ]
                    ),                    
                ]
            )
        ]
    )

