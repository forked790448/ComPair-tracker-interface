#!/usr/bin/env python3
""" This is a module for the main banner components.

    At the highest level is the "big app container"

"""
# ComPair tracker quicklook layout
# Author: Sean Griffin
#
# This module contains layout information for the ComPair tracker. 
# Based heavily on the example given here: https://dash-gallery.plotly.host/dash-manufacture-spc-dashboard/


import dash_html_components as html
import dash_core_components as dcc
import dash_bootstrap_components as dbc

def section_banner(title, className=""):
    """ Simple sectionbanner function to ease the styling.

    Parameters
    ----------
    title : str
        Banner title, name.
    className: str
        Additional class name

    Returns
    -------
    object: html.Div
        The banner Div component.
    """
    cname = "section-banner " + className
    return html.Div(className=cname, children=title)

def generate_modal():
    """ The function that creates the markdown elements for the ABOUT section.

    Returns
    -------
    markdown : html.Div
        markdown component for the ABOUT section.
    """
    return html.Div(
        id="markdown",
        className="modal",
        children=(
            html.Div(
                id="markdown-container",
                className="markdown-container",
                children=[
                    html.Div(
                        className="close-container",
                        children=html.Button(
                            "Close",
                            id="markdown_close",
                            n_clicks=0,
                            className="close-button",
                        ),
                    ),
                    html.Div(
                        className="markdown-text",
                        children=dcc.Markdown(
                            children=(
                                """
                        # ComPair Tracker Interface
                        ---------------------------
                        ###### Contact: Sambid Wasti

                        ComPair Tracker Interface dashboard to  monitor the Compair Tracker Interface.
                        Most plots are updated at about 1  Hz, the data in side them is updated either at the raw data rate
                        or at 1 Hz, depending on the specific plot. 

                        # Documentation

                        For further information, please refer to the the [tracker documentation](https://gitlab.com/mev-astronomy/ComPairAPRA/tracker/ComPair-tracker-interface/-/wikis/home).


                        
                    """
                            )
                        ),
                    ),
                ],
            )
        ),
    )


def build_banner(interface_version):
    """ Build the main banner. This is the top level banner.

    Returns
    -------
    banner : html.Div
        Top level banner. This just has the name, About button.
    """
    temp_string = 'version '+str(interface_version)
    return html.Div(
        id="top-banner",
        className="top-banner",
        children=[
            html.Div(
                id="top-banner-text",
                children=[
                    html.H1("ComPair Tracker Interface"),
                    html.H5(temp_string),
                ],
            ),
            html.Div(
                html.Button(
                        id="learn-more-button", children=[html.H6("ABOUT")], n_clicks=0
                    ),
            ),
        ],
    )