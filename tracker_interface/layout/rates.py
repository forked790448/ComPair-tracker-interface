""" Rates module.

    This is a layout module for the rates tab in the GUI application.

"""
# ComPair tracker quicklook layout
# Author: Sean Griffin
#
# This module contains layout information for the ComPair tracker. 
# Based heavily on the example given here: https://dash-gallery.plotly.host/dash-manufacture-spc-dashboard/
# July 12, 2021     Sambid K. Wasti
#                   Added extensive comments and doc-strings.
# July 16, 2021     SW: Removed unused function and commented.

import dash_html_components as html
import dash_core_components as dcc
import dash_bootstrap_components as dbc
import plotly.graph_objs as go
from . import section_banner
from . import style


def build_rate_stats_block(name ='temp'):
    """ Build the components for the row/column of the Stats for Rates"""
    return html.Div(
        [
            dbc.ListGroup(
                className = 'stats_col_3',
                horizontal=True,
                children=[
                    dbc.ListGroupItem(children='0', id =name+'0', className= 'stats_col_3_items'),
                    dbc.ListGroupItem(children='0', id =name+'1', className= 'stats_col_3_items'),
                    dbc.ListGroupItem(children='0', id =name+'2', className= 'stats_col_3_items'),
                    dbc.ListGroupItem(children='0', id =name+'3', className= 'stats_col_3_items'),
                    dbc.ListGroupItem(children='0', id =name+'4', className= 'stats_col_3_items'),
                ],
            ),
            dbc.ListGroup(
                className='stats_col_3',
                horizontal=True,
                children =[
                    dbc.ListGroupItem(children='0', id =name+'5', className= 'stats_col_3_items'),
                    dbc.ListGroupItem(children='0', id =name+'6', className= 'stats_col_3_items'),
                    dbc.ListGroupItem(children='0', id =name+'7', className= 'stats_col_3_items'),
                    dbc.ListGroupItem(children='0', id =name+'8', className= 'stats_col_3_items'),
                    dbc.ListGroupItem(children='0', id =name+'9', className= 'stats_col_3_items'),
                ]
            )
        ]
    )

def build_rate_card_burst():
    """ Build the Rates Card (Tab)

        Builds the components for the rates tab. It divides Div into 12 columns where the
        first 10 is for the plots and 2 is for the statistics.

    Returns
    -------
    rate-chart-container : html.Div
        The Div component (rate Card) that contains the rates tab.
    """
    return html.Div(
        id="rate-chart-container",
        className="twelve columns",
        children=[
            dbc.Row(
                children=[
                    dbc.Col(
                        width=9,
                        children=[
                            html.Div(
                                className= "section-banner-flex-row",
                                children=[
                                    section_banner( "Event Rates",className="section-title-left"),
                                    section_banner("  ", className="section-title-right")
                                ]
                            ),
                            html.Hr(),
                            dcc.Graph(
                                id="rate_timeseries",
                            )
                        ]
                    ),
                    dbc.Col(
                        width=3,
                        children=[
                            #section_banner("Statistics", className = "section-banner-text"),
                            build_plot_selector_box('live-rates-'),
                            html.Div(
                                className="section-banner-flex-column",
                                children=[
                                    dbc.Card(
                                        dbc.CardBody(
                                                children=[
                                                    html.H5("Rate (Hz)", className='card-stat-title'),
                                                    build_rate_stats_block('avg-rate-'),
                                                    #html.H5(id='rate-output', className='card-stat-text'),
                                                ],
                                        ),**style.card_styling_outline,
                                    ),
                                    dbc.Card(
                                            dbc.CardBody(
                                                children=[
                                                    html.H5("Maximum (Hz)", className='card-stat-title'),
                                                    build_rate_stats_block('max-rate-'),
                                                    #html.H5(id='maxrate-output', className='card-stat-text'),
                                                ],
                                            ), **style.card_styling_outline,
                                    ),
                                    dbc.Card(
                                        dbc.CardBody(
                                            children=[
                                                html.H4("Event count", className='card-title'),
                                                #html.H5(id='npackets-recv-output', className='card-text'),
                                                build_rate_stats_block('event-count-')
                                            ],
                                        ),**style.card_styling_outline,
                                    ),
                                    dbc.Card(
                                        dbc.CardBody(
                                            children=[
                                                html.H4(["Bad Packets  :  ", dbc.Badge(id = 'hist_bad_packet_count', className="ms-1")]),
                                            ],
                                         ),**style.card_styling_outline,
                                    ),
                                ]
                            ),
                        ]
                    ),
                ]
            ),
        ]
    )

def build_plot_selector_box(name='temp-'):
    """ Create a checklist box : Toggle for layer selection."""
    return html.Div(
        id = name+'plot-selector',
        #className = 'layer-checklist',
        children =[
            dcc.Checklist(
                id = name+'layer-checklist-a',
                className = 'layer-checklist',
                options=[
                    {'label': ' 0', 'value': 'L0'},
                    {'label': ' 1', 'value': 'L1'},
                    {'label': ' 2', 'value': 'L2'},
                    {'label': ' 3', 'value': 'L3'},
                    {'label': ' 4', 'value': 'L4'},
                ],
                value=['L0','L1','L2','L3','L4'],
                labelClassName='layer-checklist-label' ,
                inputClassName = 'layer-checklist-input',
            ),
            dcc.Checklist(
                id=name + 'layer-checklist-b',
                className='layer-checklist',
                options=[
                    {'label': ' 5', 'value': 'L5'},
                    {'label': ' 6', 'value': 'L6'},
                    {'label': ' 7', 'value': 'L7'},
                    {'label': ' 8', 'value': 'L8'},
                    {'label': ' 9', 'value': 'L9'},
                ],
                value=['L5', 'L6', 'L7', 'L8', 'L9'],
                labelClassName='layer-checklist-label',
                inputClassName='layer-checklist-input',
            )
        ]
    )

def build_rate_card_history():
    return html.Div(html.H1("Rates"))

def build_live_time_card():
    return html.Div(
        id ="live-livetime-container",
        className = "twelve columns",
        children=[
            dbc.Row(
                children =[
                    dbc.Col(
                        width=10,
                        children=[
                            html.Div(
                                className="section-banner-flex-row",
                                children=[
                                    section_banner("Livetime", className="section-title-left"),
                                    #section_banner(" Random Text ", className="section-title-right")
                                ]
                            ),
                            html.Hr(),
                            dcc.Graph(
                                id="live-livetime-series",
                            )
                        ]
                    )
                ]
            ),
            #html.Div(html.H1("LiveTime Plot")),
        ]
    )


def build_rate_card():
    return html.Div(
        id="rates-panel-container",
        className="twelve columns",
        children=[
            html.Hr(className='hr-zero'),
            dbc.Card(
                dbc.Tabs(
                    className='custom-mid-level-tabs',
                    children=[
                        dbc.Tab(id="rates-rates-tab",label="Live Rates", children= build_rate_card_burst(),
                                **style.mid_level_tab_styling),
                        dbc.Tab(id="rates-livetime-tab", label="LiveTime", children=build_live_time_card(),
                                **style.mid_level_tab_styling),
                        #dbc.Tab(id="rates-history-tab",label="Rates-history", children= build_rate_card_history(),
                        #        **style.mid_level_tab_styling),

                    ]
                )
            )
        ]
    )
