#!/usr/bin/env python3
""" The main layout module.

    This is the master layout that is defined in Interface.
    At the topmost level, there are 3 components, banner.build_banner(), {build_dbc_tabs(), html.Div(appconent)}
    and banner.generate_modal().
    All of the app GUI components are within the three components.


"""
# ComPair tracker quicklook layout
# Author: Sean Griffin
# This module contains layout information for the ComPair tracker. 
# Based heavily on the example given here: https://dash-gallery.plotly.host/dash-manufacture-spc-dashboard/
# July 12, 2021     Sambid K. Wasti
#                   Added extensive comments and doc-strings.


import dash_devices
from dash_devices.dependencies import Input, Output
import dash_html_components as html
import dash_core_components as dcc
import dash_bootstrap_components as dbc
import plotly.graph_objs as go
from . import banner, rates, adc, style, slowctl, top_level_tab
from . import section_banner

def build_layer_live_panel():
    return html.Div(
    id="layer-live-panel",
    className="layer-panel",
    children=[
        html.H5(children="Layer Status: "),
        html.Span(id="l0-led", className="layer-led", children=["0"]),
        html.Span(id="l1-led", className="layer-led", children=["1"]),
        html.Span(id="l2-led", className="layer-led", children=["2"]),
        html.Span(id="l3-led", className="layer-led", children=["3"]),
        html.Span(id="l4-led", className="layer-led", children=["4"]),
        html.Span(id="l5-led", className="layer-led", children=["5"]),
        html.Span(id="l6-led", className="layer-led", children=["6"]),
        html.Span(id="l7-led", className="layer-led", children=["7"]),
        html.Span(id="l8-led", className="layer-led", children=["8"]),
        html.Span(id="l9-led", className="layer-led", children=["9"]),
    ]
)

def master_layout(interface_version = 1.0):
    return html.Div(
        id='big-app-container',
        children=[
            banner.build_banner(interface_version),
            html.Hr(className='hr-zero'),
            html.Div(
                className="slim-row",
                children=[
                    html.Div(
                        dbc.Row(
                            children=[
                                dbc.Button(" Status Reset ", id="layer-status-reset",
                                           outline=True, color="dark", size="sm",
                                           className="reset-button"),
                                build_layer_live_panel(),
                            ]
                        )
                    )
                ]
            ),
            html.Div(
                id='app-container',
                children=[
                    top_level_tab.build_top_level_tabs(),
                ],
            ),
            banner.generate_modal(),
        ],
    )
