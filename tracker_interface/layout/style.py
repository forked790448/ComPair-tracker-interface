""" The style module, similar to css, for custom.
"""
#  Author: Sean Griffin
#  Just defines some CSS tab styling references.
#  July 13, 2021     Sambid K. Wasti
#                    Added extensive comments and doc-strings.



tab_styling = dict(tabClassName='boot-custom-tab', 
                   activeTabClassName='boot-custom-tab--selected')

top_level_tab_styling = dict(tabClassName='top-level-tab',
                    activeTabClassName='top-level-tab--selected',
                    labelClassName='top-level-tab-label',
                    activeLabelClassName='top-level-tab-label--selected')

mid_level_tab_styling = dict(tabClassName='mid-level-tab',
                    activeTabClassName='mid-level-tab--selected',
                    labelClassName='mid-level-tab-label',
                    activeLabelClassName='mid-level-tab-label--selected')

card_styling_outline = dict(color="secondary", outline=True)

