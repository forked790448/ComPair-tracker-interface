
import dash_html_components as html
import dash_core_components as dcc
import dash_bootstrap_components as dbc
from . import banner, rates, adc, style, slowctl, main_tab

def build_top_level_tabs():
    """ Build Top Label Tabs.

        This builds the 4 Tabs components. Main, Rates, ADC Data, Slow Control.

    Note
    ----
    | The main Tabs is has misc. overview things and each of the tabs are
    | " rates-tab ", rates.build_rate_card(),
    | " ADC-tab ", rates.build_rate_card()),
    | " slowctl-tab", rates.build_rate_card().

    Returns
    -------
    html.Div : html.Div
        A whole html.Div component containing all the dbc tabs, (Main, Rates, ADC Data, Slow Control).
    """
    return html.Div(
        id='top-level-tabs-container',
        children=[
            dbc.Card(
                dbc.Tabs(
                    id='top-level-tabs',
                    className='custom-top-level-tabs', #nav-justified
                    children=[
                        dbc.Tab(id='rates-tab', children=rates.build_rate_card(), label="Rates",
                                **style.top_level_tab_styling),  # This will be for Rates
                        dbc.Tab(id='ADC-tab', children=adc.build_adc_card(), label="ADC Data",
                                **style.top_level_tab_styling),  # This is for the ADC tab
                        dbc.Tab(id ='slowctl-tab', children=slowctl.build_slow_ctl_tab(), label="Slow Control",
                                **style.top_level_tab_styling), # This is for the ADC tab
                        #dbc.Tab(id='command-log-tab', label="Cmd Logs",
                         #       **style.top_level_tab_styling),  # This is for the ADC tab
                        #dbc.Tab(id='test-tab', children=delme3.build_delme3(), label='Tab',
                        #        **style.top_level_tab_styling),
                        #dbc.Tab(id='main-tab', children=main_tab.build_layer_live_panel(), label='Main',
                       #         **style.top_level_tab_styling),
                    ],
                ),
            )
        ]
    )