""" ADC layout module

    Creates the layout within ADC tabs.
    There are 3 subtabs within it: snapshot, history and histogram.
"""
# ComPair tracker quicklook layout
# Author: Sean Griffin
#
# This module contains layout information for the ComPair tracker. 
# Based heavily on the example given here: https://dash-gallery.plotly.host/dash-manufacture-spc-dashboard/
# July 13, 2021      Sambid K. Wasti
#                   Added extensive comments and doc-strings.
# July 16, 2021     Fixed minor typo

import dash_html_components as html
import dash_core_components as dcc
import dash_bootstrap_components as dbc
import dash_daq as ddaq
import plotly.graph_objs as go
from . import section_banner
from . import style
from . import rates

#defining valid asics and channels for the dropdown menu.
valid_layers   = [dict(label=f"{i:02d}", value=i) for i in range(9)]
valid_asics    = [dict(label=f"{i:02d}", value=i) for i in range(12)]
valid_channels = [dict(label=f"{i:02d}", value=i) for i in range(32)]

def channel_select_form(figtype):
    """ Channel-Select layout options.

        Creates the dropdown menu of asic and channel selections along with
        relevant buttons to pseudo toggle, clear and reset. As the layout is same
        for the history and histogram, the same function is used and defined by figtype.

    Parameters
    ----------
    figtype : str
        This defines the type of figure (tab). Currently, it is either history or histogram
    Returns
    -------
    'figtype'-select-form : html.Div
        The html.Div element of all the selection options.

    """
    return html.Div(
        id=figtype+'-select-form',
        #className="adc-select-form",
        children=[
            html.Br(),
            dbc.Row(
                justify='center',
                align ='left',
                children=[
                    dbc.Col(
                        align="center",
                        children=[
                            dbc.Row(
                                dbc.Col(
                                    children=[
                                        dbc.InputGroup(
                                            size="lg",
                                            children=[
                                                dbc.InputGroupAddon("LAYER", addon_type="prepend"),
                                                dbc.Select(id=figtype + '-layer-in', options=valid_layers)
                                            ]
                                        )
                                    ]
                                )
                            ),
                            dbc.Row(
                                dbc.Col(
                                    children=[
                                        dbc.InputGroup(
                                            size="lg",
                                            children=[
                                                dbc.InputGroupAddon("ASIC", addon_type="prepend"),
                                                dbc.Select(id=figtype + '-vata-in', options=valid_asics),
                                            ]
                                        )
                                    ]
                                )
                            ),
                            dbc.Row(
                                dbc.Col(
                                    children=[
                                        dbc.InputGroup(
                                            size="lg",
                                            children=[
                                                dbc.InputGroupAddon("Channel", addon_type="prepend"),
                                                dbc.Select(id=figtype + '-ch-in', options=valid_channels),
                                            ]
                                        )
                                    ]
                                )
                            ),
                        ]
                    ),
                ]
            ),
            dbc.Row(
                justify='center',
                align='center',
                children=[
                    dbc.Col(
                        #width=12,
                        children=[
                            dbc.Button("Toggle", id=figtype+'-toggle-ch', color="primary", block="true", size="lg"),   
                        ]
                    ),
                ]
            ),
            html.Br(),
            dbc.Row(
                justify='center',
                align='center',
                children=[
                    dbc.Col(
                        #width=12,
                        children=[
                            dbc.Button("Clear Channels", id=figtype+'-clear-ch', color="warning", block="true", size="lg"),   
                        ]
                    ),
                ]
            ),
            html.Br(),
            dbc.Row(
                justify='center',
                align='center',
                children=[
                    dbc.Col(
                        #width=12,
                        children=[
                            dbc.Button("Reset Data", id=figtype+'-reset-data', color="danger", block="true", size="lg"),   
                        ]
                    ),
                ]
            ),
        ]
    )

def build_plot_selector_box_xl_a(name='temp-'):
    """ Create a checklist box : Double size for each side."""
    return html.Div(
        id = name+'plot-selector-a',
        #className = 'layer-checklist',
        children =[
            dcc.Checklist(
                id = name+'layer-checklist-a',
                className = 'layer-checklist-xl',
                options=[
                    {'label': ' 0A', 'value': 'L0-a'},
                    {'label': ' 0B', 'value': 'L0-b'},
                    {'label': ' 1A', 'value': 'L1-a'},
                    {'label': ' 1B', 'value': 'L1-b'},
                    {'label': ' 2A', 'value': 'L2-a'},
                    {'label': ' 2B', 'value': 'L2-b'},
                    {'label': ' 3A', 'value': 'L3-a'},
                    {'label': ' 3B', 'value': 'L3-b'},
                    {'label': ' 4A', 'value': 'L4-a'},
                    {'label': ' 4B', 'value': 'L4-b'},
                ],
                value = [ 'L0-a','L0-b','L1-a','L1-b','L2-a','L2-b','L3-a','L3-b','L4-a','L4-b'],
                #labelStyle={'display': 'inline-block'},
                labelClassName='layer-checklist-label-xl' ,
                inputClassName = 'layer-checklist-input-xl',
            )
        ]
    )

def build_plot_selector_box_xl_b(name='temp-'):
    """ Create a checklist box : Double size for each side."""
    return html.Div(
        id = name+'plot-selector-b',
        #className = 'layer-checklist',
        children =[
            dcc.Checklist(
                id = name+'layer-checklist-b',
                className = 'layer-checklist-xl',
                options=[
                    {'label': ' 5A', 'value': 'L5-a'},
                    {'label': ' 5B', 'value': 'L5-b'},
                    {'label': ' 6A', 'value': 'L6-a'},
                    {'label': ' 6B', 'value': 'L6-b'},
                    {'label': ' 7A', 'value': 'L7-a'},
                    {'label': ' 7B', 'value': 'L7-b'},
                    {'label': ' 8A', 'value': 'L8-a'},
                    {'label': ' 8B', 'value': 'L8-b'},
                    {'label': ' 9A', 'value': 'L9-a'},
                    {'label': ' 9B', 'value': 'L9-b'},
                ],
                value = [ 'L5-a','L5-b','L6-a','L6-b','L7-a','L7-b','L8-a','L8-b','L9-a','L9-b'],
                #labelStyle={'display': 'inline-block'},
                labelClassName='layer-checklist-label-xl' ,
                inputClassName = 'layer-checklist-input-xl',
            )
        ]
    )

def snapshot_card():
    return html.Div(
        id='shapshot-adc-container',
        className='twelve columns',
        children=[
            dbc.Row(
                children=[
                    dbc.Col(
                        width=12,
                        children=[
                            html.Div(
                                className="section-banner-flex-row",
                                children=[
                                    section_banner("Layer 0-4"),
                                    html.Div(
                                        className = "snap-banner",
                                        children=[
                                            dbc.Button("Clear", id='adc-snap-reset',color='primary',
                                                  outline=True, className="snap-reset-but"),
                                            build_plot_selector_box_xl_a(name='snap-'),
                                        ]
                                    )

                                ]
                            ),
                            html.Hr(),
                            dcc.Graph(id='snapshot-adc-a',),
                        ]
                    ),
                ]
            ),
            html.Hr(),
            dbc.Row(
                children=[
                    dbc.Col(
                        width=12,
                        children=[
                            html.Div(
                                className="section-banner-flex-row",
                                children=[
                                    section_banner("Layer 5-9"),
                                    html.Div(
                                        className="snap-banner",
                                        children=[
                                            dbc.Button("Clear", id='adc-snap-reset-b', color='primary',
                                                       outline=True, className="snap-reset-but"),
                                            build_plot_selector_box_xl_b(name='snap-'),
                                        ]
                                    )

                                ]
                            ),
                            html.Hr(),
                            dcc.Graph(id='snapshot-adc-b', ),
                        ]
                    ),
                ]
            )
        ]
    )


def adc_card(figtype):
    """ Build the ADC (sub-Tab) card for the figtype

        This generates the subtab of the figtype (history and histogram). The layout is the same so this
        is used for both.

    Parameters
    ----------
    figtype : str
        Figtype is the sub-tab type we want. Its either history or histogram atm.

    Returns
    -------
    'figtype'adc-container : html.Div
        The html.Div component for the figtype (history or histogram). This is for the sub-tab of ADC tab.

    """
    return html.Div(
        id=figtype+'-adc-container',
        className='twelve columns',
        children=[
            dbc.Row(
                children=[
                    dbc.Col(
                        width=10,
                        children=[
                            html.Div(
                                className= "section-banner-flex-row",
                                children = [
                                    section_banner("Plots"),
                                    html.Div(
                                        className = "adc-yscale",
                                        children = [
                                            html.H6("YScale: "),
                                            dbc.Input(type="number", min=0,size="lg", id='ymax-'+figtype),
                                            dbc.Button("Fixed", color='dark', outline=True, id='but-fixed-' + figtype,
                                                       className="adc-yscale-but"),
                                            dbc.Button("Auto", color='dark', outline=True, id='but-auto-' + figtype,
                                                       className="adc-yscale-but")
                                        ]
                                    )
                                ]
                            ),
                            html.Hr(),
                            dcc.Graph(id=figtype+'-adc',),
                        ]
                    ),
                    dbc.Col(
                        width=2,
                        children=[
                            section_banner("Channel Select"),
                            channel_select_form(figtype),
                        ]
                    ),
                ]
            ),
            dbc.Row(
                children=[
                    dbc.Col(
                        width=12,
                        children=[
                            section_banner("Extra")
                        ]
                    ),
                ]
            )
        ]
    )

def build_adc_card():
    """ Build the ADC tab

        This is the main procedure to build the adc-tab. This creates the three subtabs
        snapshot, history and histogram.

    Returns
    -------
    adc-panel-container : html.Div
        The html.Div component that defines the whole ADC tab.
    """
    return html.Div(
        id="adc-panel-container",
        className="twelve columns",
        children=[
            html.Hr(className='hr-zero'),
            dbc.Card(
                dbc.Tabs(
                    className='custom-mid-level-tabs',
                    children=[
                        #dbc.Tab(label="History", children=adc_card("history"), **style.mid_level_tab_styling),
                        dbc.Tab(label="Snapshot",   children=snapshot_card(), **style.mid_level_tab_styling),
                        dbc.Tab(label="History",    children=adc_card("history"),**style.mid_level_tab_styling),
                        dbc.Tab(label="Histograms", children=adc_card("histogram"), **style.mid_level_tab_styling),
                    ],
            ),
        ),
        #html.Br(),
        ]
    )