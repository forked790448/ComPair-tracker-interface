#!/usr/bin/env python3
"""A test emitter to replicate a layer using a data file.

This is designed to emit data to 2 different port. Or in essence create 2 publisher
sockets. The data is generated frm 1 data file but at 2 sockets.
"""

import argparse
from datetime import timedelta
import time

from timeloop import Timeloop
import zmq

import silayer

tl = Timeloop()


class MultiPacketEmit:

    def __init__(self, data_path, packet_rate):
        """Initialize multi-packet emitter.

        Please call tl.start soon after initializing for the `tstart` to matter.

        Parameters
        ----------
        data_path: str
            Path to the raw binary file
        packet_rate: int or float
            Rate, in Hz, at which to emit packets.
        """
        self.iterator = silayer.raw2hdf.lame_byte_iterator(data_path)
        self.rate = packet_rate

        self.context = zmq.Context()
        self.socket1 = self.context.socket(zmq.PUB)
        self.socket1.setsockopt(zmq.LINGER, 1)
        self.socket1.bind("tcp://*:9998")
        print("Binded to socket.9998 running")

        self.socket2 = self.context.socket(zmq.PUB)
        self.socket2.setsockopt(zmq.LINGER, 1)
        self.socket2.bind("tcp://*:9980")
        print("Binded to socket.9980 running")

        self.faux_data_iterator = silayer.raw2hdf.lame_byte_iterator(data_path)

        self.tstart = time.time()
        self.t0 = time.time()
        self.i = 0
        self.big_tlast = self.tstart
        self.refresh_interval = packet_rate

        # register time loop job
        interval = timedelta(seconds=1./self.rate)
        tl.job(interval=interval)(self.send_packets)

    def send_packets(self):
        tnow = time.time()
        test_data1 = next(self.faux_data_iterator)
        test_data2 = next(self.faux_data_iterator)

        iterator_dt = time.time() - tnow

        socket_t0 = time.time()
        self.socket1.send(test_data1)  # , flags=zmq.NOBLOCK, copy=False)
        self.socket2.send(test_data2)
        socket_dt = time.time() - socket_t0

        # How frequently is this supposed to print???
        if self.i % self.refresh_interval == 0 and self.i > 0:
            deltaT = tnow - self.t0
            rate = 1. / deltaT
            print(
                f"{self.i} - DT: {tnow - self.t0:8.3g}",
                f"-- bigDT {tnow - self.big_tlast:8.3g}",
                f"-- socketDT: {socket_dt:8.3g}",
                f"-- iterDT: {iterator_dt:8.3g}",
                f"-- Rate: {rate:8.3g} Hz",
            )
            self.big_tlast = tnow

        self.t0 = tnow
        self.i += 1


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("packet_rate", help="Packet rate in Hz.", type=float)
    parser.add_argument("path", help="Path to data file to send.")
    args = parser.parse_args()

    emitter = MultiPacketEmit(args.path, args.packet_rate)

    tl.start(block=True)  # Run timeloop in main thread. This should cleanup on exit
