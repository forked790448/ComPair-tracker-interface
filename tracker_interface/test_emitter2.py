""" A test emitter to replicate a layer using a data file. This is designed to emit data to 2 different port.
Or in essence create 2 publisher sockets. The data is generated frm 1 data file but at 2 sockets.
"""
#!/usr/bin/env python

import time
import zmq
import silayer
import sys
from datetime import timedelta

from timeloop import Timeloop

tl = Timeloop()

if __name__ == "__main__":

    if len(sys.argv) != 3:
        print(f"Usage: python {sys.argv[0]} <packet rate in Hz> <path to data file to send>")
        exit(0)
       
    faux_data_iterator = silayer.raw2hdf.lame_byte_iterator(sys.argv[2])
    nominal_rate = float(sys.argv[1])

    context = zmq.Context()
    socket1 = context.socket(zmq.PUB)
    socket1.setsockopt(zmq.LINGER, 1)
    socket1.bind('tcp://*:9998')
    print("Binded to socket.9998 Running")

    socket2 = context.socket(zmq.PUB)
    socket2.setsockopt(zmq.LINGER, 1)
    socket2.bind('tcp://*:9980')
    print("Binded to socket.9980 Running")

    tstart = time.time()
    t0 = time.time()
    i = 0

    big_tlast = tstart
    refresh_interval = nominal_rate

    @tl.job(interval=timedelta(seconds=1./nominal_rate))
    def send_packet():
        global i, t0, refresh_interval, tstart, big_tlast

        tnow = time.time()
        test_data1 = next(faux_data_iterator)
        test_data2 = next(faux_data_iterator)

        iterator_dt = time.time() - tnow
            
        socket_t0 = time.time()
        socket1.send(test_data1)#, flags=zmq.NOBLOCK, copy=False)
        socket2.send(test_data2)
        socket_dt = time.time() - socket_t0

        if i % (refresh_interval/2) == 0 and i > 0: #Force 2 Hz
            deltaT = tnow - t0
            rate = 1.0 / (deltaT)
            #print(f"DeltaT: {deltaT:.3f} IterDT: {taf-tbef:.3g}-- Rate: {rate:.2f} Hz -- Nbytes: {len(test_data)}")
            print(f"{i} - DT: {tnow-t0:8.3g} -- bigDT {tnow-big_tlast:8.3g}-- socketDT: {socket_dt:8.3g} -- iterDT: {iterator_dt:8.3g} -- Rate: {rate:8.3g} Hz")
            big_tlast = tnow
        
        t0 = tnow
        i += 1
        
    
    tl.start()
    while True:
        try:
            time.sleep(1)
        except KeyboardInterrupt:
            tl.stop()
            break