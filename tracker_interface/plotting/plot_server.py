#!/usr/bin/env python3
""" The plot server module. This deals with the zmq sockets where 1 is a subscriber to silayer-server and
    the other is a server (iclient-server) to update plots/data/etc

    Note
    ----
    The data packets are received continuously form the silayer-server. This data is parsed using the
    "raw2hdf.simple_parse_packet" from the silayer package. This parsed data packet is used as
    a parameter and often referred as "data" in the scripts.
    The parsed packet is a dictionary with the keys as follows

    Parsed Data Packet: Dict (np.arrays)
        | **asic_event_ids:** an (n-asic,) array of event ids.
        | **asic_event_numbers:** an (n-asic,) array of event number.
        | **asic_data:** an (n-asic, 32) array of asic of channel data.
        | **asic_running_times:** an (n-asic,) long array of the asic running times.
        | **asic_live_times:** an (n-asic,) long array of the asic live times.
        | **asic_trigger_status:** an (n-asic, n-trigger-bits) long array of the FPGA-generated trigger flags.
        | **asic_packets:** List of dictionaries, with all asic fields extracted.
        | **packet_flags:** Integer representing or'd packet flags
        | **packet_time:** Timestamp from when packet was created.


"""
# ComPair tracker quicklook figure generator
# Author: Sean Griffin
#
# This module contains the figure generator and command server. S
# Should maybe be broken up into two?
#
# July 8, 2021      Sambid K. Wasti
#                   Added extensive comments and doc-strings.
# Note: Currerntly set for 1 layer. Need to expand this for 2.
# Key Notes: The updates happen only if there is data so it is defined as None and then added.

import multiprocessing as mp
import zmq
import time
from collections import deque

import plotly.express as px
import plotly.graph_objects as go
import logging

import numpy as np
import silayer.raw2hdf as raw2hdf
from . import figure_layouts as fl

import logging


LOG_LEVEL = 0
logger = logging.getLogger(__name__)
logger.setLevel(level=LOG_LEVEL)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
#ch.setFormatter(formatter)
#logger.addHandler(ch)

RATE_UPDATE_RATE = 1  # Hz
FIGURE_LOOKPACK_TIME = 20
FIGURE_LOOKBACK_SAMPLES = FIGURE_LOOKPACK_TIME * RATE_UPDATE_RATE

N_ASIC = 12
N_CH = 32
N_ADC = 1024

CHANNEL_AXIS_VALS = np.array(range(192))
N_LAYERS = 10
ALL_LEGEND_LAYERS = []
DEFAULT_LAYER_STATUS_LIST = [0,0,0,0,0,0,0,0,0,0]

class FigureGen():
    """ FigureGen Class for figure generation.

    This class initiates and describes various components needed to talk to
    the silayer-server, iserver to update, generate the plots and also to
    receive values to determine/calculate necessary measurements.


    """
    def __init__(self):
        _fields = [
            "recv_time",
            "n_recv",
            "adc_snapshots",
            "adc_histograms"
        ]
        # self.generate_empty_figures(figure_types)
        self.alloc_data()

    def alloc_data(self):
        """ This defines and allocates the various initializing value and objects.

        """
        self.tstart = time.time()
        logger.debug("Allocating memory for figure generators...")
        self.n_packets_recv = 0


        self.n_recv = deque(np.zeros(FIGURE_LOOKBACK_SAMPLES, dtype=float),
                            maxlen=FIGURE_LOOKBACK_SAMPLES)

        ## Scaling to multiple layers
        self.fpga_rates_0 = deque(np.zeros(FIGURE_LOOKBACK_SAMPLES, dtype=float),
                                maxlen=FIGURE_LOOKBACK_SAMPLES)
        self.fpga_rates_1 = deque(np.zeros(FIGURE_LOOKBACK_SAMPLES, dtype=float),
                                maxlen=FIGURE_LOOKBACK_SAMPLES)
        self.fpga_rates_2 = deque(np.zeros(FIGURE_LOOKBACK_SAMPLES, dtype=float),
                                maxlen=FIGURE_LOOKBACK_SAMPLES)
        self.fpga_rates_3 = deque(np.zeros(FIGURE_LOOKBACK_SAMPLES, dtype=float),
                                maxlen=FIGURE_LOOKBACK_SAMPLES)
        self.fpga_rates_4 = deque(np.zeros(FIGURE_LOOKBACK_SAMPLES, dtype=float),
                                maxlen=FIGURE_LOOKBACK_SAMPLES)
        self.fpga_rates_5 = deque(np.zeros(FIGURE_LOOKBACK_SAMPLES, dtype=float),
                                maxlen=FIGURE_LOOKBACK_SAMPLES)
        self.fpga_rates_6 = deque(np.zeros(FIGURE_LOOKBACK_SAMPLES, dtype=float),
                                maxlen=FIGURE_LOOKBACK_SAMPLES)
        self.fpga_rates_7 = deque(np.zeros(FIGURE_LOOKBACK_SAMPLES, dtype=float),
                                maxlen=FIGURE_LOOKBACK_SAMPLES)
        self.fpga_rates_8 = deque(np.zeros(FIGURE_LOOKBACK_SAMPLES, dtype=float),
                                maxlen=FIGURE_LOOKBACK_SAMPLES)
        self.fpga_rates_9 = deque(np.zeros(FIGURE_LOOKBACK_SAMPLES, dtype=float),
                                maxlen=FIGURE_LOOKBACK_SAMPLES)


        self.fpga_rate_time_0 = deque(np.array([0 for i in range(FIGURE_LOOKBACK_SAMPLES)]),
                                    maxlen=FIGURE_LOOKBACK_SAMPLES)
        self.fpga_rate_time_1 = deque(np.array([0 for i in range(FIGURE_LOOKBACK_SAMPLES)]),
                                      maxlen=FIGURE_LOOKBACK_SAMPLES)
        self.fpga_rate_time_2 = deque(np.array([0 for i in range(FIGURE_LOOKBACK_SAMPLES)]),
                                      maxlen=FIGURE_LOOKBACK_SAMPLES)
        self.fpga_rate_time_3 = deque(np.array([0 for i in range(FIGURE_LOOKBACK_SAMPLES)]),
                                      maxlen=FIGURE_LOOKBACK_SAMPLES)
        self.fpga_rate_time_4 = deque(np.array([0 for i in range(FIGURE_LOOKBACK_SAMPLES)]),
                                      maxlen=FIGURE_LOOKBACK_SAMPLES)
        self.fpga_rate_time_5 = deque(np.array([0 for i in range(FIGURE_LOOKBACK_SAMPLES)]),
                                      maxlen=FIGURE_LOOKBACK_SAMPLES)
        self.fpga_rate_time_6 = deque(np.array([0 for i in range(FIGURE_LOOKBACK_SAMPLES)]),
                                      maxlen=FIGURE_LOOKBACK_SAMPLES)
        self.fpga_rate_time_7 = deque(np.array([0 for i in range(FIGURE_LOOKBACK_SAMPLES)]),
                                      maxlen=FIGURE_LOOKBACK_SAMPLES)
        self.fpga_rate_time_8 = deque(np.array([0 for i in range(FIGURE_LOOKBACK_SAMPLES)]),
                                      maxlen=FIGURE_LOOKBACK_SAMPLES)
        self.fpga_rate_time_9 = deque(np.array([0 for i in range(FIGURE_LOOKBACK_SAMPLES)]),
                                      maxlen=FIGURE_LOOKBACK_SAMPLES)

        # Global counter = time, event number = Event no.
        # comment testing
        #self.fpga_globalcounter = []
        #self.fpga_event_number = []

        self.fpga_globalcounter_0 = []
        self.fpga_event_number_0 = []

        self.fpga_globalcounter_1 = []
        self.fpga_event_number_1 = []

        self.fpga_globalcounter_2 = []
        self.fpga_event_number_2 = []

        self.fpga_globalcounter_3 = []
        self.fpga_event_number_3 = []

        self.fpga_globalcounter_4 = []
        self.fpga_event_number_4 = []

        self.fpga_globalcounter_5 = []
        self.fpga_event_number_5 = []

        self.fpga_globalcounter_6 = []
        self.fpga_event_number_6 = []

        self.fpga_globalcounter_7 = []
        self.fpga_event_number_7 = []

        self.fpga_globalcounter_8 = []
        self.fpga_event_number_8 = []

        self.fpga_globalcounter_9 = []
        self.fpga_event_number_9 = []

        # ADC Snapshot/History
        self.adc_recv_time_0 = deque(np.array([0 for i in range(FIGURE_LOOKBACK_SAMPLES)]),
                                   maxlen=FIGURE_LOOKBACK_SAMPLES)
        self.adc_history_0 = deque(np.zeros((FIGURE_LOOKBACK_SAMPLES, N_ASIC, N_CH), dtype=int),
                                 maxlen=FIGURE_LOOKBACK_SAMPLES)

        self.adc_recv_time_1 = deque(np.array([0 for i in range(FIGURE_LOOKBACK_SAMPLES)]),
                                   maxlen=FIGURE_LOOKBACK_SAMPLES)
        self.adc_history_1 = deque(np.zeros((FIGURE_LOOKBACK_SAMPLES, N_ASIC, N_CH), dtype=int),
                                 maxlen=FIGURE_LOOKBACK_SAMPLES)

        self.adc_recv_time_2 = deque(np.array([0 for i in range(FIGURE_LOOKBACK_SAMPLES)]),
                                   maxlen=FIGURE_LOOKBACK_SAMPLES)
        self.adc_history_2 = deque(np.zeros((FIGURE_LOOKBACK_SAMPLES, N_ASIC, N_CH), dtype=int),
                                 maxlen=FIGURE_LOOKBACK_SAMPLES)

        self.adc_recv_time_3 = deque(np.array([0 for i in range(FIGURE_LOOKBACK_SAMPLES)]),
                                   maxlen=FIGURE_LOOKBACK_SAMPLES)
        self.adc_history_3 = deque(np.zeros((FIGURE_LOOKBACK_SAMPLES, N_ASIC, N_CH), dtype=int),
                                 maxlen=FIGURE_LOOKBACK_SAMPLES)

        self.adc_recv_time_4 = deque(np.array([0 for i in range(FIGURE_LOOKBACK_SAMPLES)]),
                                   maxlen=FIGURE_LOOKBACK_SAMPLES)
        self.adc_history_4 = deque(np.zeros((FIGURE_LOOKBACK_SAMPLES, N_ASIC, N_CH), dtype=int),
                                 maxlen=FIGURE_LOOKBACK_SAMPLES)

        self.adc_recv_time_5 = deque(np.array([0 for i in range(FIGURE_LOOKBACK_SAMPLES)]),
                                   maxlen=FIGURE_LOOKBACK_SAMPLES)
        self.adc_history_5 = deque(np.zeros((FIGURE_LOOKBACK_SAMPLES, N_ASIC, N_CH), dtype=int),
                                 maxlen=FIGURE_LOOKBACK_SAMPLES)

        self.adc_recv_time_6 = deque(np.array([0 for i in range(FIGURE_LOOKBACK_SAMPLES)]),
                                   maxlen=FIGURE_LOOKBACK_SAMPLES)
        self.adc_history_6 = deque(np.zeros((FIGURE_LOOKBACK_SAMPLES, N_ASIC, N_CH), dtype=int),
                                 maxlen=FIGURE_LOOKBACK_SAMPLES)

        self.adc_recv_time_7 = deque(np.array([0 for i in range(FIGURE_LOOKBACK_SAMPLES)]),
                                   maxlen=FIGURE_LOOKBACK_SAMPLES)
        self.adc_history_7 = deque(np.zeros((FIGURE_LOOKBACK_SAMPLES, N_ASIC, N_CH), dtype=int),
                                 maxlen=FIGURE_LOOKBACK_SAMPLES)

        self.adc_recv_time_8 = deque(np.array([0 for i in range(FIGURE_LOOKBACK_SAMPLES)]),
                                   maxlen=FIGURE_LOOKBACK_SAMPLES)
        self.adc_history_8 = deque(np.zeros((FIGURE_LOOKBACK_SAMPLES, N_ASIC, N_CH), dtype=int),
                                 maxlen=FIGURE_LOOKBACK_SAMPLES)

        self.adc_recv_time_9 = deque(np.array([0 for i in range(FIGURE_LOOKBACK_SAMPLES)]),
                                   maxlen=FIGURE_LOOKBACK_SAMPLES)
        self.adc_history_9 = deque(np.zeros((FIGURE_LOOKBACK_SAMPLES, N_ASIC, N_CH), dtype=int),
                                 maxlen=FIGURE_LOOKBACK_SAMPLES)


        self.hist_bins = np.array(range(N_ADC), dtype=int)

        self.adc_histograms = np.zeros((N_LAYERS, N_ASIC, N_CH, N_ADC), dtype=int)

        self.histogram_stats = np.zeros((N_ASIC, N_CH, 2), dtype=float) # unused atm.

        # Live time
        self.livetime_0 = deque(np.zeros((FIGURE_LOOKBACK_SAMPLES + 1), dtype=float),
                                maxlen=(FIGURE_LOOKBACK_SAMPLES + 1))
        self.livetime_1 = deque(np.zeros((FIGURE_LOOKBACK_SAMPLES + 1), dtype=float),
                                maxlen=(FIGURE_LOOKBACK_SAMPLES + 1))
        self.livetime_2= deque(np.zeros((FIGURE_LOOKBACK_SAMPLES + 1), dtype=float),
                                maxlen=(FIGURE_LOOKBACK_SAMPLES + 1))
        self.livetime_3 = deque(np.zeros((FIGURE_LOOKBACK_SAMPLES + 1), dtype=float),
                                maxlen=(FIGURE_LOOKBACK_SAMPLES + 1))
        self.livetime_4 = deque(np.zeros((FIGURE_LOOKBACK_SAMPLES + 1), dtype=float),
                                maxlen=(FIGURE_LOOKBACK_SAMPLES + 1))
        self.livetime_5 = deque(np.zeros((FIGURE_LOOKBACK_SAMPLES + 1), dtype=float),
                                maxlen=(FIGURE_LOOKBACK_SAMPLES + 1))
        self.livetime_6 = deque(np.zeros((FIGURE_LOOKBACK_SAMPLES + 1), dtype=float),
                                maxlen=(FIGURE_LOOKBACK_SAMPLES + 1))
        self.livetime_7 = deque(np.zeros((FIGURE_LOOKBACK_SAMPLES + 1), dtype=float),
                                maxlen=(FIGURE_LOOKBACK_SAMPLES + 1))
        self.livetime_8 = deque(np.zeros((FIGURE_LOOKBACK_SAMPLES + 1), dtype=float),
                                maxlen=(FIGURE_LOOKBACK_SAMPLES + 1))
        self.livetime_9 = deque(np.zeros((FIGURE_LOOKBACK_SAMPLES + 1), dtype=float),
                                maxlen=(FIGURE_LOOKBACK_SAMPLES + 1))


        self.livetime_runtime_0 = deque(np.zeros((FIGURE_LOOKBACK_SAMPLES + 1), dtype=float),
                                        maxlen=(FIGURE_LOOKBACK_SAMPLES + 1))
        self.livetime_runtime_1 = deque(np.zeros((FIGURE_LOOKBACK_SAMPLES + 1), dtype=float),
                                        maxlen=(FIGURE_LOOKBACK_SAMPLES + 1))
        self.livetime_runtime_2 = deque(np.zeros((FIGURE_LOOKBACK_SAMPLES + 1), dtype=float),
                                        maxlen=(FIGURE_LOOKBACK_SAMPLES + 1))
        self.livetime_runtime_3 = deque(np.zeros((FIGURE_LOOKBACK_SAMPLES + 1), dtype=float),
                                        maxlen=(FIGURE_LOOKBACK_SAMPLES + 1))
        self.livetime_runtime_4 = deque(np.zeros((FIGURE_LOOKBACK_SAMPLES + 1), dtype=float),
                                        maxlen=(FIGURE_LOOKBACK_SAMPLES + 1))
        self.livetime_runtime_5 = deque(np.zeros((FIGURE_LOOKBACK_SAMPLES + 1), dtype=float),
                                        maxlen=(FIGURE_LOOKBACK_SAMPLES + 1))
        self.livetime_runtime_6 = deque(np.zeros((FIGURE_LOOKBACK_SAMPLES + 1), dtype=float),
                                        maxlen=(FIGURE_LOOKBACK_SAMPLES + 1))
        self.livetime_runtime_7 = deque(np.zeros((FIGURE_LOOKBACK_SAMPLES + 1), dtype=float),
                                        maxlen=(FIGURE_LOOKBACK_SAMPLES + 1))
        self.livetime_runtime_8 = deque(np.zeros((FIGURE_LOOKBACK_SAMPLES + 1), dtype=float),
                                        maxlen=(FIGURE_LOOKBACK_SAMPLES + 1))
        self.livetime_runtime_9 = deque(np.zeros((FIGURE_LOOKBACK_SAMPLES + 1), dtype=float),
                                        maxlen=(FIGURE_LOOKBACK_SAMPLES + 1))


        self.bad_hist_packet_counter = 0

        self.frac_livetime_0 = np.zeros((FIGURE_LOOKBACK_SAMPLES), dtype=float)
        self.frac_livetime_1 = np.zeros((FIGURE_LOOKBACK_SAMPLES), dtype=float)

        self.livetime_time = deque(np.zeros(FIGURE_LOOKBACK_SAMPLES, dtype=float),
                                   maxlen=FIGURE_LOOKBACK_SAMPLES)
        self.livetime_time_0 = deque(np.zeros(FIGURE_LOOKBACK_SAMPLES, dtype=float),
                                     maxlen=FIGURE_LOOKBACK_SAMPLES)

        self.livetime_time_1 = deque(np.zeros(FIGURE_LOOKBACK_SAMPLES, dtype=float),
                                     maxlen=FIGURE_LOOKBACK_SAMPLES)

        self.layer_status_list = DEFAULT_LAYER_STATUS_LIST

        self.no_event_list = np.zeros(N_LAYERS, dtype=int)
        self.no_of_active_layers = 1

    def update_event_count(self, layer_no):
        """ A function to update the no. of event counter."""
        self.no_event_list[layer_no] += 1

    def get_event_count_string(self, temp_event_count_list):
        """ Function to get a messaging string for the event count.

        Parameters
        ----------
        temp_event_count_list :

        Returns
        -------

        """
        t_string = ''
        for x in range(len(temp_event_count_list)):
            t_string = t_string + str(temp_event_count_list[x]) + 'c'
        t_string = t_string[0:-1]
        return (t_string)

    def update_no_of_active_layers(self, temp_value):
        """ A function to update values. Currently focusing on layer status"""
        self.no_of_active_layers = temp_value

    def get_layer_status_string(self, temp_layer_status_list):
            t_string = ''
            for x in range(len(temp_layer_status_list)):
                t_string = t_string + str(temp_layer_status_list[x]) + 'c'
            t_string = t_string[0:-1]
            return (t_string)

    def update_layer_status(self, temp_value):
        """ A function to update values. Currently focusing on layer status"""
        self.layer_status_list = temp_value


    def reset_plot_buffer(self, reset_type):
        """ Resets the plot buffer.

        The plotting is done by updating the histogram or history array at the defined rate by adding at the end
        and removing the first element (which gives the animated effect. This resets the array as well as the
        statistics and outputs realted with the reset type.

        Parameters
        ----------
        reset_type : str
            Defines the type of plot to reset. It can either be "histogram" or "history".

        Returns
        -------
            None

        """
        if reset_type == "histogram":
            self.adc_histograms = np.zeros((N_LAYERS, N_ASIC, N_CH, N_ADC), dtype=int)
            self.histogram_stats = np.zeros((N_ASIC, N_CH, 2), dtype=float)  # unused atm.

        elif reset_type == "history":
            tnow = time.time() - self.tstart
            self.adc_recv_time_0 = deque(np.array([tnow for i in range(FIGURE_LOOKBACK_SAMPLES)]),
                                       maxlen=FIGURE_LOOKBACK_SAMPLES)
            self.adc_history_0 = deque(np.zeros((FIGURE_LOOKBACK_SAMPLES, N_ASIC, N_CH), dtype=int),
                                     maxlen=FIGURE_LOOKBACK_SAMPLES)
            self.adc_recv_time_1 = deque(np.array([tnow for i in range(FIGURE_LOOKBACK_SAMPLES)]),
                                       maxlen=FIGURE_LOOKBACK_SAMPLES)
            self.adc_history_1 = deque(np.zeros((FIGURE_LOOKBACK_SAMPLES, N_ASIC, N_CH), dtype=int),
                                     maxlen=FIGURE_LOOKBACK_SAMPLES)
            self.adc_recv_time_2 = deque(np.array([tnow for i in range(FIGURE_LOOKBACK_SAMPLES)]),
                                       maxlen=FIGURE_LOOKBACK_SAMPLES)
            self.adc_history_2 = deque(np.zeros((FIGURE_LOOKBACK_SAMPLES, N_ASIC, N_CH), dtype=int),
                                     maxlen=FIGURE_LOOKBACK_SAMPLES)
            self.adc_recv_time_3 = deque(np.array([tnow for i in range(FIGURE_LOOKBACK_SAMPLES)]),
                                       maxlen=FIGURE_LOOKBACK_SAMPLES)
            self.adc_history_3 = deque(np.zeros((FIGURE_LOOKBACK_SAMPLES, N_ASIC, N_CH), dtype=int),
                                     maxlen=FIGURE_LOOKBACK_SAMPLES)
            self.adc_recv_time_4 = deque(np.array([tnow for i in range(FIGURE_LOOKBACK_SAMPLES)]),
                                       maxlen=FIGURE_LOOKBACK_SAMPLES)
            self.adc_history_4 = deque(np.zeros((FIGURE_LOOKBACK_SAMPLES, N_ASIC, N_CH), dtype=int),
                                     maxlen=FIGURE_LOOKBACK_SAMPLES)
            self.adc_recv_time_5 = deque(np.array([tnow for i in range(FIGURE_LOOKBACK_SAMPLES)]),
                                       maxlen=FIGURE_LOOKBACK_SAMPLES)
            self.adc_history_5 = deque(np.zeros((FIGURE_LOOKBACK_SAMPLES, N_ASIC, N_CH), dtype=int),
                                     maxlen=FIGURE_LOOKBACK_SAMPLES)

            self.adc_recv_time_6 = deque(np.array([tnow for i in range(FIGURE_LOOKBACK_SAMPLES)]),
                                       maxlen=FIGURE_LOOKBACK_SAMPLES)
            self.adc_history_6 = deque(np.zeros((FIGURE_LOOKBACK_SAMPLES, N_ASIC, N_CH), dtype=int),
                                     maxlen=FIGURE_LOOKBACK_SAMPLES)
            self.adc_recv_time_7 = deque(np.array([tnow for i in range(FIGURE_LOOKBACK_SAMPLES)]),
                                       maxlen=FIGURE_LOOKBACK_SAMPLES)
            self.adc_history_7 = deque(np.zeros((FIGURE_LOOKBACK_SAMPLES, N_ASIC, N_CH), dtype=int),
                                     maxlen=FIGURE_LOOKBACK_SAMPLES)
            self.adc_recv_time_8 = deque(np.array([tnow for i in range(FIGURE_LOOKBACK_SAMPLES)]),
                                       maxlen=FIGURE_LOOKBACK_SAMPLES)
            self.adc_history_8 = deque(np.zeros((FIGURE_LOOKBACK_SAMPLES, N_ASIC, N_CH), dtype=int),
                                     maxlen=FIGURE_LOOKBACK_SAMPLES)
            self.adc_recv_time_9 = deque(np.array([tnow for i in range(FIGURE_LOOKBACK_SAMPLES)]),
                                       maxlen=FIGURE_LOOKBACK_SAMPLES)
            self.adc_history_9 = deque(np.zeros((FIGURE_LOOKBACK_SAMPLES, N_ASIC, N_CH), dtype=int),
                                     maxlen=FIGURE_LOOKBACK_SAMPLES)



        elif reset_type == "snapshot":
            """ Currently it is the same as history so clear history is called."""
            print("*** SNAPSHOT RESET***")
        elif reset_type == "lstatus":
            print("*** Layer Status reset***")
            self.update_layer_status([0,0,0,0,0,0,0,0,0,0])
        return




    # def update_adc_snapshots(self, data);
    def update_rates(self):
        """ This function calculates and updates the rates.

        The global and semi-global variables are updated here.

        Returns
        -------
            None

        Notes
        -----
            Now the globalcounter and evennuber are 2D list. Shpae is (*,6)
            where n is no. of events. (We need to remove the 0s)
            It finally updates a Rates List ( 2D list ) with data points [NLayers long list]
        """
        #0
        if len(self.fpga_globalcounter_0) > 0 and len(self.fpga_event_number_0) > 0:
            if len(self.fpga_globalcounter_0) == 1:
                rate = 1
            else:
                tlow = np.min(self.fpga_globalcounter_0)
                thigh = np.max(self.fpga_globalcounter_0)

                nlow = np.min(self.fpga_event_number_0)
                nhigh = np.max(self.fpga_event_number_0)

                rate = (nhigh - nlow) / (thigh - tlow)
                rate = rate / (10e-9)  # convert to seconds
        else:
            rate = 0

        tnow = time.time() - self.tstart
        self.fpga_rate_time_0.append(tnow)
        self.fpga_rates_0.append(rate)
        #1
        if len(self.fpga_globalcounter_1) > 0 and len(self.fpga_event_number_1) > 0:
            if len(self.fpga_globalcounter_1) == 1:
                rate = 1
            else:
                tlow = np.min(self.fpga_globalcounter_1)
                thigh = np.max(self.fpga_globalcounter_1)

                nlow = np.min(self.fpga_event_number_1)
                nhigh = np.max(self.fpga_event_number_1)

                rate = (nhigh - nlow) / (thigh - tlow)
                rate = rate / (10e-9)  # convert to seconds
        else:
            rate = 0

        self.fpga_rate_time_1.append(tnow)
        self.fpga_rates_1.append(rate)
        #2
        if len(self.fpga_globalcounter_2) > 0 and len(self.fpga_event_number_2) > 0:
            if len(self.fpga_globalcounter_2) == 1:
                rate = 1
            else:
                tlow = np.min(self.fpga_globalcounter_2)
                thigh = np.max(self.fpga_globalcounter_2)

                nlow = np.min(self.fpga_event_number_2)
                nhigh = np.max(self.fpga_event_number_2)

                rate = (nhigh - nlow) / (thigh - tlow)
                rate = rate / (10e-9)  # convert to seconds
        else:
            rate = 0

        self.fpga_rate_time_2.append(tnow)
        self.fpga_rates_2.append(rate)
        #3
        if len(self.fpga_globalcounter_3) > 0 and len(self.fpga_event_number_3) > 0:
            if len(self.fpga_globalcounter_3) == 1:
                rate = 1
            else:
                tlow = np.min(self.fpga_globalcounter_3)
                thigh = np.max(self.fpga_globalcounter_3)

                nlow = np.min(self.fpga_event_number_3)
                nhigh = np.max(self.fpga_event_number_3)

                rate = (nhigh - nlow) / (thigh - tlow)
                rate = rate / (10e-9)  # convert to seconds
        else:
            rate = 0

        self.fpga_rate_time_3.append(tnow)
        self.fpga_rates_3.append(rate)

        #4
        if len(self.fpga_globalcounter_4) > 0 and len(self.fpga_event_number_4) > 0:
            if len(self.fpga_globalcounter_4) == 1:
                rate = 1
            else:
                tlow = np.min(self.fpga_globalcounter_4)
                thigh = np.max(self.fpga_globalcounter_4)

                nlow = np.min(self.fpga_event_number_4)
                nhigh = np.max(self.fpga_event_number_4)

                rate = (nhigh - nlow) / (thigh - tlow)
                rate = rate / (10e-9)  # convert to seconds
        else:
            rate = 0

        self.fpga_rate_time_4.append(tnow)
        self.fpga_rates_4.append(rate)

        #5
        if len(self.fpga_globalcounter_5) > 0 and len(self.fpga_event_number_5) > 0:
            if len(self.fpga_globalcounter_5) == 1:
                rate = 1
            else:
                tlow = np.min(self.fpga_globalcounter_5)
                thigh = np.max(self.fpga_globalcounter_5)

                nlow = np.min(self.fpga_event_number_5)
                nhigh = np.max(self.fpga_event_number_5)

                rate = (nhigh - nlow) / (thigh - tlow)
                rate = rate / (10e-9)  # convert to seconds
        else:
            rate = 0

        self.fpga_rate_time_5.append(tnow)
        self.fpga_rates_5.append(rate)

        #6
        if len(self.fpga_globalcounter_6) > 0 and len(self.fpga_event_number_6) > 0:
            if len(self.fpga_globalcounter_6) == 1:
                rate = 1
            else:
                tlow = np.min(self.fpga_globalcounter_6)
                thigh = np.max(self.fpga_globalcounter_6)

                nlow = np.min(self.fpga_event_number_6)
                nhigh = np.max(self.fpga_event_number_6)

                rate = (nhigh - nlow) / (thigh - tlow)
                rate = rate / (10e-9)  # convert to seconds
        else:
            rate = 0
        self.fpga_rate_time_6.append(tnow)
        self.fpga_rates_6.append(rate)


        #7
        if len(self.fpga_globalcounter_7) > 0 and len(self.fpga_event_number_7) > 0:
            if len(self.fpga_globalcounter_7) == 1:
                rate = 1
            else:
                tlow = np.min(self.fpga_globalcounter_7)
                thigh = np.max(self.fpga_globalcounter_7)

                nlow = np.min(self.fpga_event_number_7)
                nhigh = np.max(self.fpga_event_number_7)

                rate = (nhigh - nlow) / (thigh - tlow)
                rate = rate / (10e-9)  # convert to seconds
        else:
            rate = 0
        self.fpga_rate_time_7.append(tnow)
        self.fpga_rates_7.append(rate)

        #8
        if len(self.fpga_globalcounter_8) > 0 and len(self.fpga_event_number_8) > 0:
            if len(self.fpga_globalcounter_8) == 1:
                rate = 1
            else:
                tlow = np.min(self.fpga_globalcounter_8)
                thigh = np.max(self.fpga_globalcounter_8)

                nlow = np.min(self.fpga_event_number_8)
                nhigh = np.max(self.fpga_event_number_8)

                rate = (nhigh - nlow) / (thigh - tlow)
                rate = rate / (10e-9)  # convert to seconds
        else:
            rate = 0
        self.fpga_rate_time_8.append(tnow)
        self.fpga_rates_8.append(rate)

        #9
        if len(self.fpga_globalcounter_9) > 0 and len(self.fpga_event_number_9) > 0:
            if len(self.fpga_globalcounter_9) == 1:
                rate = 1
            else:
                tlow = np.min(self.fpga_globalcounter_9)
                thigh = np.max(self.fpga_globalcounter_9)

                nlow = np.min(self.fpga_event_number_9)
                nhigh = np.max(self.fpga_event_number_9)

                rate = (nhigh - nlow) / (thigh - tlow)
                rate = rate / (10e-9)  # convert to seconds
        else:
            rate = 0
        self.fpga_rate_time_9.append(tnow)
        self.fpga_rates_9.append(rate)

        #----

        self.fpga_globalcounter_0 = []
        self.fpga_event_number_0 = []
        self.fpga_globalcounter_1 = []
        self.fpga_event_number_1 = []
        self.fpga_globalcounter_2 = []
        self.fpga_event_number_2 = []
        self.fpga_globalcounter_3 = []
        self.fpga_event_number_3 = []
        self.fpga_globalcounter_4 = []
        self.fpga_event_number_4 = []
        self.fpga_globalcounter_5 = []
        self.fpga_event_number_5 = []
        self.fpga_globalcounter_6 = []
        self.fpga_event_number_6 = []
        self.fpga_globalcounter_7 = []
        self.fpga_event_number_7 = []
        self.fpga_globalcounter_8 = []
        self.fpga_event_number_8 = []
        self.fpga_globalcounter_9 = []
        self.fpga_event_number_9 = []


    def update_livetime(self):
        """

        Returns
        -------

        """
        tnow = time.time() - self.tstart
        self.livetime_time.append(tnow)  # this can be same for all layers.

        t_array = []
        for i in range(FIGURE_LOOKBACK_SAMPLES):
            t_lt = self.livetime_0[i + 1] - self.livetime_0[i]
            t_rt = self.livetime_runtime_0[i + 1] - self.livetime_runtime_0[i]

            if t_rt <= 0:
                t_frac_lt = 0
            else:
                t_frac_lt = float(t_lt) / float(t_rt)

            t_array.append(t_frac_lt)
        self.frac_livetime_0 = np.array(t_array)

        t_array = []
        for i in range(FIGURE_LOOKBACK_SAMPLES):
            t_lt = self.livetime_1[i + 1] - self.livetime_1[i]
            t_rt = self.livetime_runtime_1[i + 1] - self.livetime_runtime_1[i]

            if t_rt <= 0:
                t_frac_lt = 0
            else:
                t_frac_lt = float(t_lt) / float(t_rt)

            t_array.append(t_frac_lt)
        self.frac_livetime_1 = np.array(t_array)

    def update_snapshots(self, data, layer):
        """ Update the snapshot arrays.

        Update (Append) additional data to the array responsible for the snapshot and history plots.
        This is skipped for the first packet.

        Parameters
        ----------
        data : parsed data packet
            simple parsed data packet.

        """
        #print('******* plotserver: update snapshot', layer)
        if data is not None:
            if layer == 0:
                #print('################UPDATE SNAPSHOT CHECK')
                self.adc_recv_time_0.append(time.time() - self.tstart)
                self.adc_history_0.append(data['channel_data'])

            elif layer == 1:
                self.adc_recv_time_1.append(time.time() - self.tstart)
                self.adc_history_1.append(data['channel_data'])

            elif layer == 2:
                self.adc_recv_time_2.append(time.time() - self.tstart)
                self.adc_history_2.append(data['channel_data'])

            elif layer == 3:
                self.adc_recv_time_3.append(time.time() - self.tstart)
                self.adc_history_3.append(data['channel_data'])

            elif layer == 4:
                self.adc_recv_time_4.append(time.time() - self.tstart)
                self.adc_history_4.append(data['channel_data'])

            elif layer == 5:
                self.adc_recv_time_5.append(time.time() - self.tstart)
                self.adc_history_5.append(data['channel_data'])

            elif layer == 6:
                self.adc_recv_time_6.append(time.time() - self.tstart)
                self.adc_history_6.append(data['channel_data'])

            elif layer == 7:
                self.adc_recv_time_7.append(time.time() - self.tstart)
                self.adc_history_7.append(data['channel_data'])

            elif layer == 8:
                self.adc_recv_time_8.append(time.time() - self.tstart)
                self.adc_history_8.append(data['channel_data'])

            elif layer == 9:
                self.adc_recv_time_9.append(time.time() - self.tstart)
                self.adc_history_9.append(data['channel_data'])

    def update_histograms(self, data, layer):
        """ Update the histogram arrays.

        Update (Append) additional data to the array responsible for the histogram plots.
        This is skipped for the first packet

        Parameters
        ----------
        data : parsed data packet
            simple parsed data packet.
        """
        temp_flag = False
        if data is not None:
            for ai, asic in enumerate(data['channel_data']):
                for ch, adc in enumerate(asic):
                    # adding some checks.
                    if 0<=layer<10 and 0<=ai<12 and 0<=ch<32:
                        self.adc_histograms[layer,ai, ch, adc] += 1
                    else:
                        temp_flag = True

        if temp_flag == True:
            self.bad_hist_packet_counter += 1

    def update_event_info(self, data_packet, layer_no=0):
        """ Update the event information for the running rate calculation.

        Update (Append) additional data to the array responsible for the histogram plots.
        This is skipped for the first packet

        Parameters
        ----------
        data : parsed data packet
            simple parsed data packet.
        layer_no: int
            layer no. Default = 0

        Notes
        -----
            Updating it for the 2 layers.
            the global counter and even number is 2D now.

        """
        if data_packet is not None:
            if layer_no == 0:
                self.fpga_globalcounter_0.append(data_packet['running_time'])
                self.fpga_event_number_0.append(data_packet['event_count'])
                self.livetime_0.append(data_packet['live_time'])
                self.livetime_runtime_0.append(data_packet['running_time'])

            elif layer_no == 1:
                self.fpga_globalcounter_1.append(data_packet['running_time'])
                self.fpga_event_number_1.append(data_packet['event_count'])
                self.livetime_1.append(data_packet['live_time'])
                self.livetime_runtime_1.append(data_packet['running_time'])

            elif layer_no == 2:
                self.fpga_globalcounter_2.append(data_packet['running_time'])
                self.fpga_event_number_2.append(data_packet['event_count'])
                self.livetime_2.append(data_packet['live_time'])
                self.livetime_runtime_2.append(data_packet['running_time'])

            elif layer_no == 3:
                self.fpga_globalcounter_3.append(data_packet['running_time'])
                self.fpga_event_number_3.append(data_packet['event_count'])
                self.livetime_3.append(data_packet['live_time'])
                self.livetime_runtime_3.append(data_packet['running_time'])

            elif layer_no == 4:
                self.fpga_globalcounter_4.append(data_packet['running_time'])
                self.fpga_event_number_4.append(data_packet['event_count'])
                self.livetime_4.append(data_packet['live_time'])
                self.livetime_runtime_4.append(data_packet['running_time'])

            elif layer_no == 5:
                self.fpga_globalcounter_5.append(data_packet['running_time'])
                self.fpga_event_number_5.append(data_packet['event_count'])
                self.livetime_5.append(data_packet['live_time'])
                self.livetime_runtime_5.append(data_packet['running_time'])

            elif layer_no == 6:
                self.fpga_globalcounter_6.append(data_packet['running_time'])
                self.fpga_event_number_6.append(data_packet['event_count'])
                self.livetime_6.append(data_packet['live_time'])
                self.livetime_runtime_6.append(data_packet['running_time'])

            elif layer_no == 7:
                self.fpga_globalcounter_7.append(data_packet['running_time'])
                self.fpga_event_number_7.append(data_packet['event_count'])
                self.livetime_7.append(data_packet['live_time'])
                self.livetime_runtime_7.append(data_packet['running_time'])

            elif layer_no == 8:
                self.fpga_globalcounter_8.append(data_packet['running_time'])
                self.fpga_event_number_8.append(data_packet['event_count'])
                self.livetime_8.append(data_packet['live_time'])
                self.livetime_runtime_8.append(data_packet['running_time'])

            elif layer_no == 9:
                self.fpga_globalcounter_9.append(data_packet['running_time'])
                self.fpga_event_number_9.append(data_packet['event_count'])
                self.livetime_9.append(data_packet['live_time'])
                self.livetime_runtime_9.append(data_packet['running_time'])

    def generate_val(self, val_type):
        """ Generate a value asked.

        Currently set for n-packets and live-time.

        Parameters
        ----------
        val_type : str
            Value type asked.

        Returns
        -------
        generated_value : int
            generated asked value.
        """
        if val_type == 'n-packets':
            #return self.n_packets_recv
            return self.get_event_count_string(self.no_event_list)

        elif val_type == 'layer-status-str':
            return self.get_layer_status_string(self.layer_status_list)

        elif val_type == 'bad-hist-packet-counter':
            return str(self.bad_hist_packet_counter)

        else:
            return -99

    def generate_fig(self, fig_type):
        """ Generates a figure asked.

        Parameters
        ----------
        fig_type : str
            Type of figure asked. There are 4 kinds. rate, adc-snapshot, adc-history, adc-hist.

        Returns
        -------
        fig: figure object
            returns the figure depending on the fig_type asked
        """
        #print('****GEN FIG***')
        fig = go.Figure()           #plotly graph object. (not sure why its not just plotly)

        if fig_type == 'livetime':
            x_val = list(self.livetime_time)
            y_val = list(self.frac_livetime_0)
            #print(y_val)
            fig.add_trace(go.Scatter(x=x_val, y=y_val, name="L0"))

            y_val = list(self.frac_livetime_1)
            fig.add_trace(go.Scatter(x=x_val, y=y_val, name="L1"))

            fig.data[0].update(mode='markers+lines')  # Line Style
            fig.update_layout(fl.livetime_layout)

        elif fig_type == 'rate':

            #fig = delme4.sample_fig()
            x_val = list(self.fpga_rate_time_0)
            y_val = list(self.fpga_rates_0)
            fig.add_trace(go.Scatter(x=x_val, y=y_val, name = "L0",marker={'color' : px.colors.qualitative.Plotly[0]}))
            fig.data[0].update(mode='markers+lines')  # Line Style
            fig.update_layout(fl.rate_layout)
            fig.update_yaxes(automargin=True)


            if self.no_of_active_layers > 1:
                x_val = list(self.fpga_rate_time_1)
                y_val = list(self.fpga_rates_1)
                fig.add_trace(go.Scatter(x=x_val, y=y_val, name="L1",marker={'color' : px.colors.qualitative.Plotly[1]}))
                fig.data[0].update(mode='markers+lines')  # Line Style
                fig.update_layout(fl.rate_layout)
                fig.update_yaxes(automargin=True)

                x_val = list(self.fpga_rate_time_2)
                y_val = list(self.fpga_rates_2)
                fig.add_trace(go.Scatter(x=x_val, y=y_val, name="L2",marker={'color' : px.colors.qualitative.Plotly[2]}))
                fig.data[0].update(mode='markers+lines')  # Line Style
                fig.update_layout(fl.rate_layout)
                fig.update_yaxes(automargin=True)

                x_val = list(self.fpga_rate_time_3)
                y_val = list(self.fpga_rates_3)
                fig.add_trace(go.Scatter(x=x_val, y=y_val, name="L3",marker={'color' : px.colors.qualitative.Plotly[3]}))
                fig.data[0].update(mode='markers+lines')  # Line Style
                fig.update_layout(fl.rate_layout)
                fig.update_yaxes(automargin=True)

                x_val = list(self.fpga_rate_time_4)
                y_val = list(self.fpga_rates_4)
                fig.add_trace(go.Scatter(x=x_val, y=y_val, name="L4",marker={'color' : px.colors.qualitative.Plotly[4]}))
                fig.data[0].update(mode='markers+lines')  # Line Style
                fig.update_layout(fl.rate_layout)
                fig.update_yaxes(automargin=True)

                x_val = list(self.fpga_rate_time_5)
                y_val = list(self.fpga_rates_5)
                fig.add_trace(go.Scatter(x=x_val, y=y_val, name="L5",marker={'color' : px.colors.qualitative.Plotly[5]}))
                fig.data[0].update(mode='markers+lines')  # Line Style
                fig.update_layout(fl.rate_layout)
                fig.update_yaxes(automargin=True)

                x_val = list(self.fpga_rate_time_6)
                y_val = list(self.fpga_rates_6)
                fig.add_trace(go.Scatter(x=x_val, y=y_val, name="L6",marker={'color' : px.colors.qualitative.Plotly[6]}))
                fig.data[0].update(mode='markers+lines')  # Line Style
                fig.update_layout(fl.rate_layout)
                fig.update_yaxes(automargin=True)

                x_val = list(self.fpga_rate_time_7)
                y_val = list(self.fpga_rates_7)
                fig.add_trace(go.Scatter(x=x_val, y=y_val, name="L7",marker={'color' : px.colors.qualitative.Plotly[7]}))
                fig.data[0].update(mode='markers+lines')  # Line Style
                fig.update_layout(fl.rate_layout)
                fig.update_yaxes(automargin=True)

                x_val = list(self.fpga_rate_time_8)
                y_val = list(self.fpga_rates_8)
                fig.add_trace(go.Scatter(x=x_val, y=y_val, name="L8",marker={'color' : px.colors.qualitative.Plotly[8]}))
                fig.data[0].update(mode='markers+lines')  # Line Style
                fig.update_layout(fl.rate_layout)
                fig.update_yaxes(automargin=True)

                x_val = list(self.fpga_rate_time_9)
                y_val = list(self.fpga_rates_9)
                fig.add_trace(go.Scatter(x=x_val, y=y_val, name="L9",marker={'color' : px.colors.qualitative.Plotly[9]}))
                fig.data[0].update(mode='markers+lines')  # Line Style
                fig.update_layout(fl.rate_layout)
                fig.update_yaxes(automargin=True)


        elif fig_type == 'adc-snapshot-a':

            #L0
            darr = np.resize(self.adc_history_0[-1], (12 * 32))
            fig.add_trace(go.Scatter(x=CHANNEL_AXIS_VALS, y=darr[0:6 * 32],
                                     name="L0 SideA", line_shape='hvh',marker={'color' : px.colors.qualitative.Plotly[0]})
                          )
            fig.add_trace(go.Scatter(x=CHANNEL_AXIS_VALS, y=darr[6 * 32::],
                                     name="L0 SideB", line_shape='hvh',marker={'color' : px.colors.qualitative.Plotly[1]})
                          )
            fig.update_layout(fl.adc_snapshot_layout)
            fig.update_yaxes(dict(rangemode='tozero')) # fixing the lower axis to zero.

            #L1
            darr = np.resize(self.adc_history_1[-1], (12 * 32))
            fig.add_trace(go.Scatter(x=CHANNEL_AXIS_VALS, y=darr[0:6 * 32],
                                     name="L1 SideA", line_shape='hvh',marker={'color' : px.colors.qualitative.Plotly[2]})
                          )
            fig.add_trace(go.Scatter(x=CHANNEL_AXIS_VALS, y=darr[6 * 32::],
                                     name="L1 SideB", line_shape='hvh',marker={'color' : px.colors.qualitative.Plotly[3]})
                          )

            # L2
            darr = np.resize(self.adc_history_2[-1], (12 * 32))
            fig.add_trace(go.Scatter(x=CHANNEL_AXIS_VALS, y=darr[0:6 * 32],
                                     name="L2 SideA", line_shape='hvh',marker={'color' : px.colors.qualitative.Plotly[4]})
                          )
            fig.add_trace(go.Scatter(x=CHANNEL_AXIS_VALS, y=darr[6 * 32::],
                                     name="L2 SideB", line_shape='hvh',marker={'color' : px.colors.qualitative.Plotly[5]})
                          )

            # L3
            darr = np.resize(self.adc_history_3[-1], (12 * 32))
            fig.add_trace(go.Scatter(x=CHANNEL_AXIS_VALS, y=darr[0:6 * 32],
                                     name="L3 SideA", line_shape='hvh',marker={'color' : px.colors.qualitative.Plotly[6]})
                          )
            fig.add_trace(go.Scatter(x=CHANNEL_AXIS_VALS, y=darr[6 * 32::],
                                     name="L3 SideB", line_shape='hvh',marker={'color' : px.colors.qualitative.Plotly[7]})
                          )

            # L4
            darr = np.resize(self.adc_history_4[-1], (12 * 32))
            fig.add_trace(go.Scatter(x=CHANNEL_AXIS_VALS, y=darr[0:6 * 32],
                                     name="L4 SideA", line_shape='hvh',marker={'color' : px.colors.qualitative.Plotly[8]})
                          )
            fig.add_trace(go.Scatter(x=CHANNEL_AXIS_VALS, y=darr[6 * 32::],
                                     name="L4 SideB", line_shape='hvh',marker={'color' : px.colors.qualitative.Plotly[9]})
                          )


            #add the vertical line to separate each asics.
            fig.add_vline(x=31,line_width=1.5, line_dash="dash",line_color="black")
            fig.add_vline(x=63, line_width=1.5, line_dash="dash", line_color="black")
            fig.add_vline(x=95, line_width=1.5, line_dash="dash", line_color="black")
            fig.add_vline(x=127, line_width=1.5, line_dash="dash", line_color="black")
            fig.add_vline(x=159, line_width=1.5, line_dash="dash", line_color="black")
            #fig.update_layout(fl.adc_snapshot_layout)

        elif fig_type == 'adc-snapshot-b':
            # L5
            darr = np.resize(self.adc_history_5[-1], (12 * 32))
            fig.add_trace(go.Scatter(x=CHANNEL_AXIS_VALS, y=darr[0:6 * 32],
                                     name="L5 SideA", line_shape='hvh',
                                     marker={'color': px.colors.qualitative.Plotly[0]})
                          )
            fig.add_trace(go.Scatter(x=CHANNEL_AXIS_VALS, y=darr[6 * 32::],
                                     name="L5 SideB", line_shape='hvh',
                                     marker={'color': px.colors.qualitative.Plotly[1]})
                          )
            fig.update_layout(fl.adc_snapshot_layout)
            fig.update_yaxes(dict(rangemode='tozero'))  # fixing the lower axis to zero.

            # L6
            darr = np.resize(self.adc_history_6[-1], (12 * 32))
            fig.add_trace(go.Scatter(x=CHANNEL_AXIS_VALS, y=darr[0:6 * 32],
                                     name="L6 SideA", line_shape='hvh',
                                     marker={'color': px.colors.qualitative.Plotly[2]})
                          )
            fig.add_trace(go.Scatter(x=CHANNEL_AXIS_VALS, y=darr[6 * 32::],
                                     name="L6 SideB", line_shape='hvh',
                                     marker={'color': px.colors.qualitative.Plotly[3]})
                          )

            # L7
            darr = np.resize(self.adc_history_7[-1], (12 * 32))
            fig.add_trace(go.Scatter(x=CHANNEL_AXIS_VALS, y=darr[0:6 * 32],
                                     name="L7 SideA", line_shape='hvh',
                                     marker={'color': px.colors.qualitative.Plotly[4]})
                          )
            fig.add_trace(go.Scatter(x=CHANNEL_AXIS_VALS, y=darr[6 * 32::],
                                     name="L7 SideB", line_shape='hvh',
                                     marker={'color': px.colors.qualitative.Plotly[5]})
                          )

            # L8
            darr = np.resize(self.adc_history_8[-1], (12 * 32))
            fig.add_trace(go.Scatter(x=CHANNEL_AXIS_VALS, y=darr[0:6 * 32],
                                     name="L8 SideA", line_shape='hvh',
                                     marker={'color': px.colors.qualitative.Plotly[6]})
                          )
            fig.add_trace(go.Scatter(x=CHANNEL_AXIS_VALS, y=darr[6 * 32::],
                                     name="L8 SideB", line_shape='hvh',
                                     marker={'color': px.colors.qualitative.Plotly[7]})
                          )

            # L9
            darr = np.resize(self.adc_history_9[-1], (12 * 32))
            fig.add_trace(go.Scatter(x=CHANNEL_AXIS_VALS, y=darr[0:6 * 32],
                                     name="L9 SideA", line_shape='hvh',
                                     marker={'color': px.colors.qualitative.Plotly[8]})
                          )
            fig.add_trace(go.Scatter(x=CHANNEL_AXIS_VALS, y=darr[6 * 32::],
                                     name="L9 SideB", line_shape='hvh',
                                     marker={'color': px.colors.qualitative.Plotly[9]})
                          )

            #add the vertical line to separate each asics.
            fig.add_vline(x=31,line_width=1.5, line_dash="dash",line_color="black")
            fig.add_vline(x=63, line_width=1.5, line_dash="dash", line_color="black")
            fig.add_vline(x=95, line_width=1.5, line_dash="dash", line_color="black")
            fig.add_vline(x=127, line_width=1.5, line_dash="dash", line_color="black")
            fig.add_vline(x=159, line_width=1.5, line_dash="dash", line_color="black")


        elif fig_type.startswith('history'):
            # format: "adc-hist_11c21,5c0,7c12"
            plotting_list = fig_type.split(":")[1]          #Splits the vata pair strings.
            #fig = delme4.sample_fig()
            #print('***** plot gen history: Check1: ',plotting_list)
            if len(plotting_list) != 0:                     # Making sure its not empty.
                plotting_list = plotting_list.split(',')    # get a vata pair string array.

                for bigpair in plotting_list:
                    layer, pair = bigpair.split('l')
                    vata, ch = pair.split('c')

                    layer=int(layer)
                    vata = int(vata)
                    ch = int(ch)

                    #print(layer,' ',vata,' ',ch)
                    temp_time_array = None
                    temp_layer_array = None
                    if layer == 0:
                        temp_time_array = self.adc_recv_time_0
                        temp_layer_array = self.adc_history_0
                    elif layer == 1:
                        temp_time_array = self.adc_recv_time_1
                        temp_layer_array = self.adc_history_1
                    elif layer == 2:
                        temp_time_array = self.adc_recv_time_2
                        temp_layer_array = self.adc_history_2
                    elif layer == 3:
                        temp_time_array = self.adc_recv_time_3
                        temp_layer_array = self.adc_history_3
                    elif layer == 4:
                        temp_time_array = self.adc_recv_time_4
                        temp_layer_array = self.adc_history_4
                    elif layer == 5:
                        temp_time_array = self.adc_recv_time_5
                        temp_layer_array = self.adc_history_5
                    elif layer == 6:
                        temp_time_array = self.adc_recv_time_6
                        temp_layer_array = self.adc_history_6
                    elif layer == 7:
                        temp_time_array = self.adc_recv_time_7
                        temp_layer_array = self.adc_history_7
                    elif layer == 8:
                        temp_time_array = self.adc_recv_time_8
                        temp_layer_array = self.adc_history_8
                    elif layer == 9:
                        temp_time_array = self.adc_recv_time_9
                        temp_layer_array = self.adc_history_9
                    else:
                        logger.error(f"Requested Layer out of bounds: {layer}")

                    logger.debug(f"Getting ADC History for L{layer:02d}A{vata:02d}-{ch:02d}")

                    if vata < 0 or vata >= N_ASIC:
                        logger.error(f"Requested an ASIC out of bounds: {vata}.")
                    elif ch < 0 or ch >= N_CH:
                        logger.error(f"Requested an ASIC out of bounds: {ch}.")
                    else:
                        fig.add_trace(go.Scatter(x=list(temp_time_array),
                                                 y=np.array(temp_layer_array)[:, vata, ch],
                                                 name=f"L{layer:02d}-A{vata:02d}-{ch:02d}"))

            fig.update_layout(fl.adc_history_layout)

        elif fig_type.startswith('adc-hist'):
            # format: "adc-hist:11c21,5c0,7c12"
            # this has been updated with the layer.
            # Get list of specified channels.
            plotting_list = fig_type.split(":")[1]

            if len(plotting_list) != 0:
                plotting_list = plotting_list.split(',')
                for bigpair in plotting_list:
                    layer, pair = bigpair.split('l')
                    vata, ch = pair.split('c')

                    layer=int(layer)
                    vata = int(vata)
                    ch = int(ch)

                    logger.debug(f"Getting ADC Histogram for L{layer:02d}A{vata:02d}-{ch:02d}")
                    if vata < 0 or vata >= N_ASIC:
                        logger.error(f"Requested an ASIC out of bounds: {vata}.")
                    elif ch < 0 or ch >= N_CH:
                        logger.error(f"Requested an ASIC out of bounds: {ch}.")
                    else:
                        fig.add_trace(
                            go.Scatter(x=self.hist_bins, y=self.adc_histograms[layer][vata][ch], name=f"L{layer:02d}-A{vata:02d}-{ch:02d}",
                                       line_shape='hvh'))

            fig.update_layout(fl.adc_hist_layout)

        return fig

#def data_recv_loop(ctrl_socket, data_socket0, data_socket1):
def data_recv_loop(ctrl_socket, data_socket_arr):
    """ The data recieve (secondary) loop for the iserver and subscriber.

    This is the data receiving loop that deals with the iserver (plot-server) and also deals with data
    packets received from the silayer-server.

    Parameters
    ----------
    ctrl_socket : socket id
        Control socket for the iserver (REP).
    data_socket : socket id
        Data socket id for receiving the data packet (SUB)

    """
    #print('step5')
    poller = zmq.Poller()                       # poller handles multiple socket.
    poller.register(ctrl_socket, zmq.POLLIN)
    for socket in data_socket_arr:
        poller.register(socket,zmq.POLLIN)

    figures = FigureGen()                       # initialize the figure class

    # Set up time for rate generation
    tlast = figures.tstart
    data_packet0 = None
    data_packet1 = None
    data_packet2 = None
    data_packet3 = None
    data_packet4 = None
    data_packet5 = None
    data_packet6 = None
    data_packet7 = None
    data_packet8 = None
    data_packet9 = None


    n_last_update = figures.n_packets_recv
    figures.update_no_of_active_layers(len(data_socket_arr))
    while True:
        socks = dict(poller.poll())
        if ctrl_socket in socks:
            message = ctrl_socket.recv_string()
           #print('****CONTROL SOCKET REcieved Message', message)
            logger.debug(f"Received ctrl message {message}")
            if message == "stop":
                ctrl_socket.send_string("OK!")
                break
            elif message.startswith("gimme_a_plot"):
                # ctrl_socket.send_pyobj("OK22!")
                #print('**** plotserver: Asked for a plot')
                fig_type = message.split("_")[-1]
                logger.debug(f"A plot was requested of type: {fig_type}")
                ctrl_socket.send_pyobj(figures.generate_fig(fig_type))

            elif message.startswith("gimme_a_val"):
                value_type = message.split("_")[-1]
                logger.debug(f"A value was requested of type: {value_type}")
                ctrl_socket.send_pyobj(figures.generate_val(value_type))

            elif message.startswith("reset"):

                reset_type = message.split("_")[-1]
                print(f"MESSAGE passed through:  f{reset_type}")
                # Valid options are: histogram, history, snapshot

                #logger.debug(f"Received a request to reset data for {reset_type}")

                figures.reset_plot_buffer(reset_type)

                ctrl_socket.send_string("OK!")

            else:
                ctrl_socket.send_string("UNRECOGNIZED")

        tnow = time.time()
        temp_layer_status_array = figures.layer_status_list

        if data_socket_arr[0] in socks:
            data0 = data_socket_arr[0].recv()
            data_packet0 = raw2hdf.simple_parse_packet(data0)
            temp_layer_status_array[0] = 1
            figures.n_packets_recv += 1
            figures.update_event_count(0)
            figures.update_histograms(data_packet0,0)
            figures.update_event_info(data_packet0,0)

        if figures.no_of_active_layers > 1 :
            if data_socket_arr[1] in socks:
                data1 = data_socket_arr[1].recv()
                data_packet1 = raw2hdf.simple_parse_packet(data1)
                temp_layer_status_array[1] = 1
                figures.n_packets_recv += 1
                figures.update_event_count(1)
                figures.update_histograms(data_packet1,1)
                figures.update_event_info(data_packet1,1)

            if data_socket_arr[2] in socks:
                data2 = data_socket_arr[2].recv()
                data_packet2 = raw2hdf.simple_parse_packet(data2)
                temp_layer_status_array[2] = 1
                figures.n_packets_recv += 1
                figures.update_event_count(2)
                figures.update_histograms(data_packet2,2)
                figures.update_event_info(data_packet2,2)

            if data_socket_arr[3] in socks:
                data3 = data_socket_arr[3].recv()
                data_packet3 = raw2hdf.simple_parse_packet(data3)
                temp_layer_status_array[3] = 1
                figures.n_packets_recv += 1
                figures.update_event_count(3)
                figures.update_histograms(data_packet3,3)
                figures.update_event_info(data_packet3,3)

            if data_socket_arr[4] in socks:
                data4 = data_socket_arr[4].recv()
                data_packet4 = raw2hdf.simple_parse_packet(data4)
                temp_layer_status_array[4] = 1
                figures.n_packets_recv += 1
                figures.update_event_count(4)
                figures.update_histograms(data_packet4,4)
                figures.update_event_info(data_packet4,4)

            if data_socket_arr[5] in socks:
                data5 = data_socket_arr[5].recv()
                data_packet5 = raw2hdf.simple_parse_packet(data5)
                temp_layer_status_array[5] = 1
                figures.n_packets_recv += 1
                figures.update_event_count(5)
                figures.update_histograms(data_packet5,5)
                figures.update_event_info(data_packet5,5)

            if data_socket_arr[6] in socks:
                data6 = data_socket_arr[6].recv()
                data_packet6 = raw2hdf.simple_parse_packet(data6)
                temp_layer_status_array[6] = 1
                figures.n_packets_recv += 1
                figures.update_event_count(6)
                figures.update_histograms(data_packet6,6)
                figures.update_event_info(data_packet6,6)

            if data_socket_arr[7] in socks:
                data7 = data_socket_arr[7].recv()
                data_packet7 = raw2hdf.simple_parse_packet(data7)
                temp_layer_status_array[7] = 1
                figures.n_packets_recv += 1
                figures.update_event_count(7)
                figures.update_histograms(data_packet7,7)
                figures.update_event_info(data_packet7,7)

            if data_socket_arr[8] in socks:
                data8 = data_socket_arr[8].recv()
                data_packet8 = raw2hdf.simple_parse_packet(data8)
                temp_layer_status_array[8] = 1
                figures.n_packets_recv += 1
                figures.update_event_count(8)
                figures.update_histograms(data_packet8,8)
                figures.update_event_info(data_packet8,8)

            if data_socket_arr[9] in socks:
                data9 = data_socket_arr[9].recv()
                data_packet9 = raw2hdf.simple_parse_packet(data9)
                temp_layer_status_array[9] = 1
                figures.n_packets_recv += 1
                figures.update_event_count(9)
                figures.update_histograms(data_packet9,9)
                figures.update_event_info(data_packet9,9)


        # Update the rate plot at ~1/RATE_UPDATE_RATE Hz.
        # Periodically update (Rates and snapshot) depending on the
        # defined RATE which is 1 by default. Also update the no. of packet received value.

        if tnow - tlast > 1. / RATE_UPDATE_RATE:
            figures.update_rates()
            figures.update_livetime()
            tlast = tnow
            figures.update_layer_status(temp_layer_status_array)

            # If we've received new data, then update the snapshots
            # why not do this in the data socket.
            if n_last_update < figures.n_packets_recv:
                figures.update_snapshots(data_packet0, 0)
                figures.update_snapshots(data_packet1, 1)
                figures.update_snapshots(data_packet2, 2)
                figures.update_snapshots(data_packet3, 3)
                figures.update_snapshots(data_packet4, 4)
                figures.update_snapshots(data_packet5, 5)
                figures.update_snapshots(data_packet6, 6)
                figures.update_snapshots(data_packet7, 7)
                figures.update_snapshots(data_packet8, 8)
                figures.update_snapshots(data_packet9, 9)


            n_last_update = figures.n_packets_recv

def change_log_level(log_level):
    """
    Change the global log level.
    Parameters
    ----------
    log_level : int
        log level value translated from run.py
    """
    global LOG_LEVEL
    LOG_LEVEL = log_level

def main(ctrl_port, host="localhost", data_port=9998 , log_level = LOG_LEVEL):
    """ Main function (primary loop) for the plot server (zmq REP Server : iserver).

    This is the main function where we have a REP server defined and also the subscription socket
    defined to subscribe to the silayer-server where continuous packets are received. This is initiated
    at start of the program to define the socket and start the server and again when it is stopped in the
    end.

    Parameters
    ----------
    ctrl_port : port
        control port that the iserver socket is binded to. Default input value randomed between the ports
        "tcp://", min_port=64000, max_port=65000.

    host : host
        control host. Default is "localhost".

    data_port : data port
        data port. Default is 9998
    """

    change_log_level(log_level) #translate the log level.
    No_Active_Layers = len(host)
    ctx = zmq.Context()
    ctrl_socket = ctx.socket(zmq.REP)
    ctrl_socket.connect(f"tcp://localhost:{ctrl_port}")
    logger.info(f"Plot server started. ctrl_port: {ctrl_port} ; data source: {43487}:{data_port}")


    while True:
        logger.debug("Waiting for control signal...")
        ctrl_msg = ctrl_socket.recv_string()
        logger.debug(f"Received {ctrl_msg}")

        raw_data_socket_arr = []
        raw_data_server_arr = []
        if ctrl_msg.startswith("connect"):
            # Making it work for N layer.

            if No_Active_Layers >= 1:
                raw_data_server_addr_0 = f"tcp://{host[0]}:{data_port[0]}"
                logger.info(f"Connecting to data server at {raw_data_server_addr_0}")
                raw_data_socket_0 = ctx.socket(zmq.SUB)
                raw_data_socket_0.setsockopt_string(zmq.SUBSCRIBE, "")
                raw_data_socket_0.connect(raw_data_server_addr_0)
                raw_data_socket_arr.append(raw_data_socket_0)
                raw_data_server_arr.append(raw_data_server_addr_0)

            if No_Active_Layers >= 2:
                raw_data_server_addr_1 = f"tcp://{host[1]}:{data_port[1]}"
                logger.info(f"Connecting to data server at {raw_data_server_addr_1}")
                raw_data_socket_1 = ctx.socket(zmq.SUB)
                raw_data_socket_1.setsockopt_string(zmq.SUBSCRIBE, "")
                raw_data_socket_1.connect(raw_data_server_addr_1)
                raw_data_socket_arr.append(raw_data_socket_1)
                raw_data_server_arr.append(raw_data_server_addr_1)

            if No_Active_Layers >= 3:
                raw_data_server_addr_2 = f"tcp://{host[2]}:{data_port[2]}"
                logger.info(f"Connecting to data server at {raw_data_server_addr_2}")
                raw_data_socket_2 = ctx.socket(zmq.SUB)
                raw_data_socket_2.setsockopt_string(zmq.SUBSCRIBE, "")
                raw_data_socket_2.connect(raw_data_server_addr_2)
                raw_data_socket_arr.append(raw_data_socket_2)
                raw_data_server_arr.append(raw_data_server_addr_2)

            if No_Active_Layers >= 4:
                raw_data_server_addr_3 = f"tcp://{host[3]}:{data_port[3]}"
                logger.info(f"Connecting to data server at {raw_data_server_addr_3}")
                raw_data_socket_3 = ctx.socket(zmq.SUB)
                raw_data_socket_3.setsockopt_string(zmq.SUBSCRIBE, "")
                raw_data_socket_3.connect(raw_data_server_addr_3)
                raw_data_socket_arr.append(raw_data_socket_3)
                raw_data_server_arr.append(raw_data_server_addr_3)

            if No_Active_Layers >= 5:
                raw_data_server_addr_4 = f"tcp://{host[4]}:{data_port[4]}"
                logger.info(f"Connecting to data server at {raw_data_server_addr_4}")
                raw_data_socket_4 = ctx.socket(zmq.SUB)
                raw_data_socket_4.setsockopt_string(zmq.SUBSCRIBE, "")
                raw_data_socket_4.connect(raw_data_server_addr_4)
                raw_data_socket_arr.append(raw_data_socket_4)
                raw_data_server_arr.append(raw_data_server_addr_4)

            if No_Active_Layers >= 6:
                raw_data_server_addr_5 = f"tcp://{host[5]}:{data_port[5]}"
                logger.info(f"Connecting to data server at {raw_data_server_addr_5}")
                raw_data_socket_5= ctx.socket(zmq.SUB)
                raw_data_socket_5.setsockopt_string(zmq.SUBSCRIBE, "")
                raw_data_socket_5.connect(raw_data_server_addr_5)
                raw_data_socket_arr.append(raw_data_socket_5)
                raw_data_server_arr.append(raw_data_server_addr_5)

            if No_Active_Layers >= 7:
                raw_data_server_addr_6 = f"tcp://{host[6]}:{data_port[6]}"
                logger.info(f"Connecting to data server at {raw_data_server_addr_6}")
                raw_data_socket_6= ctx.socket(zmq.SUB)
                raw_data_socket_6.setsockopt_string(zmq.SUBSCRIBE, "")
                raw_data_socket_6.connect(raw_data_server_addr_6)
                raw_data_socket_arr.append(raw_data_socket_6)
                raw_data_server_arr.append(raw_data_server_addr_6)

            if No_Active_Layers >= 8:
                raw_data_server_addr_7 = f"tcp://{host[7]}:{data_port[7]}"
                logger.info(f"Connecting to data server at {raw_data_server_addr_7}")
                raw_data_socket_7= ctx.socket(zmq.SUB)
                raw_data_socket_7.setsockopt_string(zmq.SUBSCRIBE, "")
                raw_data_socket_7.connect(raw_data_server_addr_7)
                raw_data_socket_arr.append(raw_data_socket_7)
                raw_data_server_arr.append(raw_data_server_addr_7)

            if No_Active_Layers >= 9:
                raw_data_server_addr_8 = f"tcp://{host[8]}:{data_port[8]}"
                logger.info(f"Connecting to data server at {raw_data_server_addr_8}")
                raw_data_socket_8= ctx.socket(zmq.SUB)
                raw_data_socket_8.setsockopt_string(zmq.SUBSCRIBE, "")
                raw_data_socket_8.connect(raw_data_server_addr_8)
                raw_data_socket_arr.append(raw_data_socket_8)
                raw_data_server_arr.append(raw_data_server_addr_8)

            if No_Active_Layers >= 10:
                raw_data_server_addr_9 = f"tcp://{host[9]}:{data_port[9]}"
                logger.info(f"Connecting to data server at {raw_data_server_addr_9}")
                raw_data_socket_9= ctx.socket(zmq.SUB)
                raw_data_socket_9.setsockopt_string(zmq.SUBSCRIBE, "")
                raw_data_socket_9.connect(raw_data_server_addr_9)
                raw_data_socket_arr.append(raw_data_socket_9)
                raw_data_server_arr.append(raw_data_server_addr_9)


            ctrl_socket.send_string(f"Connected to {raw_data_server_arr}")
            logger.info("Connected; entering data receive loop.")

            data_recv_loop(ctrl_socket, raw_data_socket_arr)
            #data_recv_loop(ctrl_socket, raw_data_socket_0,raw_data_socket_1 )

        elif ctrl_msg == "exit":
            ctrl_socket.send("Fine.")
            break
        else:
            ctrl_socket.send("Unknown message!")

    logger.info("Exiting plot server.")
