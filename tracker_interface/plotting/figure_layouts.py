#!/usr/bin/env python3
""" **Plot styling.**

    Plot styling similar to css but for plotly layouts.
    Uses the plotly update_layout to update the plot styling.

    Note
    ----
    There are 5 style sets:
        | **default_font_layout**  : plot font, color, background color.
        | **rate_layout**          : rate plot layout.
        | **adc_snapshot_layout**  : adc tab: snapshot plot layout.
        | **adc_history_layout**   : adc tab: history plot layout.
        | **adc_hist_layout**      : adc tab: histogram plot layout.

"""
# ComPair tracker quicklook plot styling
# Author: Sean Griffin
#
# This module contains information for plot styling.
# July 9, 2021      Sambid K. Wasti
#                   Added extensive comments and doc-strings.


import plotly.express as px

#Font Styling
default_font_layout = dict(
    font=dict(
        family="Times New Roman",
        size=18,
        #color="RebeccaPurple"
        #color="dark",
    ),
    paper_bgcolor='rgba(0,0,0,0)',
    plot_bgcolor='rgba(0,0,0,0)'
    )


livetime_layout = dict(
    xaxis_title="elapsed time (seconds)",
    yaxis_title="Live time (frac)",
    margin=dict(l=10, r=10, b=0, t=0, pad=4),
    yaxis_range=[0,1],
    **default_font_layout
)

#Rates Tab plot Styling
rate_layout = dict(
    xaxis_title="Elapsed Time (s)",
    yaxis_title="Rate (Hz)",
    margin=dict(l=10, r=10, b=0,t=0,pad=4),
    autosize=False,
    **default_font_layout
)

#Available Tab layout parameters to change.
#ADC Tab, Snapshot plot Styling
adc_snapshot_layout = dict(
     xaxis_title="channel",
     yaxis_title="adc (LSB)",
     showlegend=True,
     legend=dict(
         orientation="h",
         yanchor="top",
         y=1.02,
     ),     
    margin=dict(l=10, r=10, b=0,t=0,pad=4),
     **default_font_layout
)
''' Available plot layout parameters to change.
'''
#ADC Tab, Histogram plot Styling
adc_hist_layout = dict(
     xaxis_title="adc (LSB)",
     yaxis_title="number of counts",
     showlegend=True,
     legend=dict(
         orientation="h",
         yanchor="top",
         y=1.02,
        
     ),     
    margin=dict(l=10, r=10, b=0,t=0,pad=4),     
     **default_font_layout
)

#ADC Tab, history plot Styling
adc_history_layout = dict(
     xaxis_title="elapsed time (seconds)",
     yaxis_title="adc (LSB)",
     showlegend=True,
     #legend=dict(
     #    orientation="h",
     #    yanchor="top",
     #    y=1.02,
        
     #),     
    margin=dict(l=10, r=10, b=0,t=0,pad=4),     
     **default_font_layout
)
