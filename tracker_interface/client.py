#!/usr/bin/env python3
""" iclinet API module.

    Update this: Module to interact with Plot-server (iserver) This has an iclient (from callbacks)
    that talks with the iserver.

"""
# ComPair tracker quicklook command client. 
# Author: Sean Griffin
#
# This module is for interacting with the plot generator/Server.
# July 9, 2021      Sambid K. Wasti
#                   Added extensive comments and doc-strings


import os
import time
import json
import multiprocessing as mp
import zmq

from plotting import plot_server

import logging

CTRL_HOSTS = "localhost"
DATA_PORTS = 9998
SERVER_PORT = 5556


LOG_LEVEL = 0
logger = logging.getLogger(__name__)
logger.setLevel(level=LOG_LEVEL)
#ch = logging.StreamHandler()
#ch.setLevel(level=LOG_LEVEL)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')



class Client:
    """Class for interacting with plot generation server (iserver).

        This initializes a REQ client (iclient) to talk with iserver.

    """

    #Initializaiton.
    def __init__(
            self,
            hosts=CTRL_HOSTS,
            server_port=SERVER_PORT,
            data_ports=DATA_PORTS,
            n_thread_context=1,
            log_level=LOG_LEVEL,
    ):
        """ Initialization of iclient.

        Parameters
        ----------
        host : control host
        server_port : server port
        data_port : data port
        n_thread_context : no. of thread for multi-processing.default 1.
        """
        self.hosts = hosts
        self.server_port = server_port
        self.data_ports = data_ports
        self.n_thread_context = n_thread_context
        self.log_level = log_level

        self.change_log_level(log_level)
        self.ctx = zmq.Context(n_thread_context)

        #Set up the plot generator thread and communication socket. (Local Client)
        self.recv_ctrl_sock = self.ctx.socket(zmq.REQ)
        ctrl_port = self.recv_ctrl_sock.bind_to_random_port(
            "tcp://*", min_port=64000, max_port=65000
        )

        #Create and start Multi-process and initiate plot_server.main
        logger.info(f"Connecting to server on port {ctrl_port}")
        self.recv_proc = mp.Process(
            target=plot_server.main,
            args=(ctrl_port,),
            kwargs={"host": hosts, "data_port": data_ports, "log_level":log_level},
        )

        self.recv_proc.daemon = True  # Force process to die when parent process does.
        self.recv_proc.start()  # Start the command receiver process.


    def recv_val(self, val_type='n_packets_recv'):
        """ Receive value from the iserver.

        Receive the value defined by the requested type from the iserver (plotserver)

        Parameters
        ----------
        val_type : str (opt)
            String that defines the type of value requested. Default is 'n_packet_recv'

        Returns
        -------
        val : str
            The asked value.

        """
        logger.debug(f"Asking for a value...")                      # logging
        self.recv_ctrl_sock.send_string(f"gimme_a_val_{val_type}")  # send a message to iserver.
        val = self.recv_ctrl_sock.recv_pyobj()                      # get the pyobj asked defined by message.
        logger.debug(f"Got it.")                                    # logging
        return val

    def recv_plot(self, fig_type='rate'):
        """ Receive the plot requested.

        Receive the plot defined by the requested type from the iserver (plotserver)

        Parameters
        ----------
        fig_type : str (opt)
            String that defines the type of figure requested. Default is rate.

        Returns
        -------
        plot : figure
            Returns the figure object requested.

        """
        logger.debug(f"Asking for plot..."+fig_type)
        self.recv_ctrl_sock.send_string(f"gimme_a_plot_{fig_type}")
        plot = self.recv_ctrl_sock.recv_pyobj()
        logger.debug(f"Got it.")
        return plot

    def send_recv(self, message):
        """ Send and receive message (to/from) iserver.

        Parameters
        ----------
        message : str
            message string.

        Returns
        -------
        message : str
            message string.

        """
        logger.info(f"Sending message {message}")
        self.recv_ctrl_sock.send_string(message)            # Sending message.
        return self.recv_ctrl_sock.recv_string()            # Receive and return message.

    def change_log_level(self, log_level):
        """
        Change the logging set level.

        Parameters
        ----------
        log_level : int
            Entered Log Level to update. The current values are set in run.py and translated accordingly.
        """
        global LOG_LEVEL
        LOG_LEVEL = log_level

def main():
    """ Main function.
        Initialize client and connect to the iserver and get the plot.
    """
    logger.info("Connecting to client processes...")
    client = Client()
    setlog = client.send_recv("setlog")
    response = client.send_recv("connect")
    logger.debug(f"{response}")
    for i in range(10):
        response = client.recv_plot()
        print(f"{response}")
        time.sleep(0.05)

    logger.info("Sending stop command.")
    response = client.send_recv("stop")
    logger.info(f"{response}")

    return


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description="Run the data receiver")
    parser.add_argument("port", type=int, help="Port number to use for command socket")
    parser.add_argument("--host", type=str, default="silayer.local", help="Silicon layer hostnname")
    parser.add_argument("--data-port", type=int, dest="dport", default=9998, help="Silayer port number.")
    args = parser.parse_args()

    logger.setLevel(level=logging.DEBUG)
    #ch = logging.StreamHandler()
    #ch.setLevel(level=logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    #ch.setFormatter(formatter)
    #logger.addHandler(ch)

    try:
        logger.info("Starting main porcess.")
        main()

    except KeyboardInterrupt:
        logger.info("Exiting...")
