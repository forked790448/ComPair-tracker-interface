#!/usr/bin/env python3
""" @@@@The interface module that handles most of the GUI interface.
    Decorators are skipped by sphinx atm therefore some callbacks
    are missing.

    The missing callbacks during initialization are:
    | callback_shared: set_history_list : Update/clear history plots.
    | callback_shared: set_histogram_list : update/clear histogram plots.
    | callback_connect: func : runs and initiates the timer function whenever client changes.
    This means, it stops when we close the tab on the browser and starts when we open it.


"""
# ComPair tracker quicklook Dash interface
# Author: Sean Griffin
#
#
# This is the master Dash interface class; it does a lot.
# July 11, 2021     Sambid K. Wasti
#                   Added extensive comments and doc-strings.

import dash_devices
from dash_devices.dependencies import Input, Output, State
from dash_devices.exceptions import PreventUpdate
from threading import Timer
from callbacks import register_callbacks
import layout
from layout.master import master_layout
import numpy as np
import plotly.express as px
import os               #To execute system commands.
import subprocess       #To execute system commands.
import logging

# Logger setting.
LOG_LEVEL = 0
logger = logging.getLogger(__name__)
logger.setLevel(level=LOG_LEVEL)
#ch = logging.StreamHandler()
#ch.setLevel(level=LOG_LEVEL)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')


def update_layer_status_class_string(layer_stat_str):
    """ This takes in the array and defines the className for the layer status

    Parameters
    ----------
    layer_stat_array : str
        String of layer status.

    Returns
    -------
    layer_status_str: str list
        list of layer status classname
    """
    layer_status_str_array = ['layer-led', 'layer-led', 'layer-led', 'layer-led', 'layer-led',
                              'layer-led', 'layer-led', 'layer-led', 'layer-led', 'layer-led']
    temp_list_arr = layer_stat_str.split('c')

    for i in range(len(temp_list_arr)): # layer status array
        if temp_list_arr[i] == '0':
            layer_status_str_array[i] = 'layer-led-off'
        elif temp_list_arr[i] == '1':
            layer_status_str_array[i] = 'layer-led-on'

    return layer_status_str_array


def update_vata_pairs(layer, vata, ch,  existing):
    """ @@@Update the selected data pairs (ASIC and Channel) to emulate toggle.

        This takes in the data pair and update the data list.
        If the pair exists in the list, it removes it, if it does exist,it appends it to the list.
        In order to emulate toggle.

    Parameters
    ----------
    vata : str/int
        selected ASIC no.
    ch : str/int
        selected channel no.
    existing : list
        list array of vata pairs.

    Returns
    -------
    return_str: str array
        Specially formatted string array for plotting. The format is "A##-@@" where ## is asic no. and @@ is channel
        number. Example: A02-13 would refer to asic2, channel13.
    existing: list array
        Updated list array.
    """
    vata = int(vata)
    ch = int(ch)
    layer = int(layer)
    pair = (layer, vata, ch)

    new_pair = True
    # run a loop for things in existing.
    # index goes from 0 last index for existing.
    # exist are elements (2d array with vata,chan) in existing.
    #    Again: each item exist is a 2d object.
    # It removes the pair, if it already exist.
    for index, exist in enumerate(existing):
        if exist[0] == layer and exist[1] == vata and exist[2] == ch: # if exist
            existing.pop(index)     # Removes the item in the list at index
            new_pair = False
            break
    # Or appends the pair if it doesnt. Note: No order.
    if new_pair:
        existing.append(pair)

    # The return string: Special string with values for plotting.
    # Current string format: A##-@@ where ##=ASIC, @@chan
    # example A02-13 = asic2, chan 13
    # The special string is created everytime the update is initiated.
    # However, the special return_string is not used. The existing array is used.
    return_str = ""
    for pair in existing: 
        return_str += f"A{pair[0]:02d}-{pair[1]:02d}, "

    return_str = return_str[0:-2] #Cut the trailing comma
    #print('-------- Interface.py Update Vata Pairs---',return_str)
    return [return_str, existing]

def make_plot_gen_str(channels):
    """ Generate the special string for the plots.

    Parameters
    ----------
    channels : list 2D array
        2D list. The list is {(a1,b1),(a2,b2)...} where a1 is asic no. and b1 is channel no.

    Returns
    -------
    pair_str = str
        Special string containing the pair. The format would be as 'acb,a1cb1..' where a is asic no.
        b is channel no. and c is just a character.
        NOTE: THis is modifying it to layer asic channel. so its XlYcZ where x = layer, y= asic and z = channel.
    """
    pair_str = ""
    for pair in channels:
        #print(pair)
        pair_str += f"{pair[0]}l{pair[1]}c{pair[2]},"

    #print('------------INTERFACE:Make Plot Gen Str :', pair_str)
    return pair_str[0:-1]


class Interface:
    interface_version = 1.0
    def __init__(self, app, client, interface_version=interface_version,log_level=LOG_LEVEL):

        self.client = client
        self.histogram_plot_channels = []
        self.adc_history_plot_channels = []
        self.interface_version = interface_version
        self.app = app
        self.app.layout = master_layout(interface_version=interface_version)
        self.rates_live_rates_layer_select =[True, True, True, True, True, True,True, True, True, True] # For 10
        self.adc_snap_layer_select_a = [True, True, True, True, True, True, True, True, True, True, True, True, True,
                                        True, True, True]
        self.adc_snap_layer_select_b = [True, True, True, True, True, True, True, True, True, True, True, True, True,
                                        True, True, True]

        self.log_level = log_level

        self.change_log_level(log_level)

        register_callbacks(self) #Add some callbacks to the app.
        self.ctx = dash_devices.callback_context

        @self.app.callback_shared(
            None
            , 
            [
                Input('history-toggle-ch', 'n_clicks'), 
                Input('history-clear-ch', 'n_clicks'),
            ],
            [
                State('history-layer-in', 'value'),
                State('history-vata-in', 'value'),
                State('history-ch-in', 'value'),
            ]

            )
        def set_history_list(toggle_nclicks, clear_nclicks, layer, vata, ch):
            """ Callback: Emulate a toggle using clicks and selection to update history list

                The function is used to emulate a toggle switch for adding/removing a data set( for a
                pair of asic and chan). It also uses a clear-button to clear all the data set.
                This uses callback_Context that prevents update when nothing selected. This has state,
                which is shown in example 6,7. This is used when both the vata in and channel in are updated
                before the toggle.

            Parameters
            ----------
            toggle_nclicks : Input
                history-toggle-channel clicked
            clear_nclicks : Input
                history clear channel click
            vata : State
                vata value
            ch : State
                channel value.

            Returns
            -------
            None. But updates the adc_history_plot_channels array.
            """
            # Input two buttons clicked, history and clear.
            return_str = ''
            if not self.ctx.triggered:                          # if nothing changed (button clicked)
                return_str = "No buttons clicked."
                self.adc_history_plot_channels = []
            else:                                               # if change happens (button clicked)
                triggered = self.ctx.triggered[0]['prop_id'].split('.')[0]  # get the id of clicked button.
                if triggered == 'history-toggle-ch':            # if button history.
                    if vata is None or ch is None:              # if no selection
                        raise PreventUpdate

                    # else
                    return_str, existing = update_vata_pairs( layer, vata, ch, self.adc_history_plot_channels)
                    self.adc_history_plot_channels = existing # collection of arrays (3units) but with string.
                    #print('------ Callback Updated list', return_str, self.adc_history_plot_channels)
                elif triggered == 'history-clear-ch':           #if button clicked is clear.
                    return_str = "No channels selected."
                    self.adc_history_plot_channels = []

            return

        @self.app.callback_shared(
            None
            ,
            [
                Input('histogram-toggle-ch', 'n_clicks'), 
                Input('histogram-clear-ch', 'n_clicks')
            ],

            [
                State('histogram-layer-in', 'value'),
                State('histogram-vata-in', 'value'), 
                State('histogram-ch-in', 'value'),            
            ]
            
            )
        def set_histogram_list(toggle_n, clear_n, layer, vata, ch):
            return_str = ""
            if not self.ctx.triggered:
                return_str = "No buttons clicked."
                self.histogram_plot_channels = []
            else:
                triggered = self.ctx.triggered[0]['prop_id'].split('.')[0]  # get the id of the button clicked
                if triggered == 'histogram-toggle-ch':                      # if its histogram button
                    if vata is None or ch is None:                          # if no asic/chan selected, skip
                        raise PreventUpdate
                    #else udpate the array
                    return_str, existing = update_vata_pairs(layer,vata, ch, self.histogram_plot_channels)
                    self.adc_history_plot_channels = existing
                elif triggered == 'histogram-clear-ch':                     # if clear button clicked
                    return_str = "No channels selected."
                    self.histogram_plot_channels = []                       #empty the array

            return

        @self.app.callback(
            None,
            [Input(component_id='live-rates-layer-checklist-a', component_property='value')]
        )
        def update_live_rates_layer(options_chosen):
            """ Update the Layer selection array for Live Rates tab when selector changes."""
            self.rates_live_rates_layer_select[0] = (True if 'L0' in options_chosen else False)
            self.rates_live_rates_layer_select[1] = (True if 'L1' in options_chosen else False)
            self.rates_live_rates_layer_select[2] = (True if 'L2' in options_chosen else False)
            self.rates_live_rates_layer_select[3] = (True if 'L3' in options_chosen else False)
            self.rates_live_rates_layer_select[4] = (True if 'L4' in options_chosen else False)

        @self.app.callback(
            None,
            [Input(component_id='live-rates-layer-checklist-b', component_property='value')]
        )
        def update_live_rates_layer(options_chosen):
            """ Update the Layer selection array for Live Rates tab when selector changes."""
            self.rates_live_rates_layer_select[5] = (True if 'L5' in options_chosen else False)
            self.rates_live_rates_layer_select[6] = (True if 'L6' in options_chosen else False)
            self.rates_live_rates_layer_select[7] = (True if 'L7' in options_chosen else False)
            self.rates_live_rates_layer_select[8] = (True if 'L8' in options_chosen else False)
            self.rates_live_rates_layer_select[9] = (True if 'L9' in options_chosen else False)

        @self.app.callback(
            None,
            [Input(component_id='snap-layer-checklist-a', component_property='value')]
        )
        def update_snapshot_layer(options_chosen):
            """ Update the Layer selection array for Live Rates tab when selector changes."""
            self.adc_snap_layer_select_a[0] = (True if 'L0-a' in options_chosen else False)
            self.adc_snap_layer_select_a[1] = (True if 'L0-b' in options_chosen else False)
            self.adc_snap_layer_select_a[2] = (True if 'L1-a' in options_chosen else False)
            self.adc_snap_layer_select_a[3] = (True if 'L1-b' in options_chosen else False)
            self.adc_snap_layer_select_a[4] = (True if 'L2-a' in options_chosen else False)
            self.adc_snap_layer_select_a[5] = (True if 'L2-b' in options_chosen else False)
            self.adc_snap_layer_select_a[6] = (True if 'L3-a' in options_chosen else False)
            self.adc_snap_layer_select_a[7] = (True if 'L3-b' in options_chosen else False)
            self.adc_snap_layer_select_a[8] = (True if 'L4-a' in options_chosen else False)
            self.adc_snap_layer_select_a[9] = (True if 'L4-b' in options_chosen else False)

        @self.app.callback(
            None,
            [Input(component_id='snap-layer-checklist-b', component_property='value')]
        )
        def update_snapshot_layer(options_chosen):
            """ Update the Layer selection array for Live Rates tab when selector changes."""
            self.adc_snap_layer_select_b[0] = (True if 'L5-a' in options_chosen else False)
            self.adc_snap_layer_select_b[1] = (True if 'L5-b' in options_chosen else False)
            self.adc_snap_layer_select_b[2] = (True if 'L6-a' in options_chosen else False)
            self.adc_snap_layer_select_b[3] = (True if 'L6-b' in options_chosen else False)
            self.adc_snap_layer_select_b[4] = (True if 'L7-a' in options_chosen else False)
            self.adc_snap_layer_select_b[5] = (True if 'L7-b' in options_chosen else False)
            self.adc_snap_layer_select_b[6] = (True if 'L8-a' in options_chosen else False)
            self.adc_snap_layer_select_b[7] = (True if 'L8-b' in options_chosen else False)
            self.adc_snap_layer_select_b[8] = (True if 'L9-a' in options_chosen else False)
            self.adc_snap_layer_select_b[9] = (True if 'L9-b' in options_chosen else False)

        @self.app.callback_connect
        def func(client, connect):
            """ This is triggered whenever the client connection changes. (example2,2a,3,3a).
            """
            print(client, connect, len(app.clients))
            if connect and len(app.clients)==1 and self.client is not None:
                self.timer_callback()
            elif not connect and len(app.clients)==0:
                self.timer.cancel()

    def change_log_level(self, log_level):
        """
        Change the global log level.
        Parameters
        ----------
        log_level : int
            log level value translated from run.py
        """
        global LOG_LEVEL
        LOG_LEVEL = log_level

    def timer_callback(self):
        """ Continuous function to trigger.

            @@@@Pseudo timer callback function. This is to emulate a function that triggers continuously
            on timer. Currently, it "refreshes" every 0.7s which is set here.
            @@@Might add a value to update this number?
            This function calls itself so it is a recursive function.

            The rate is generated by comparing the mean from 4 of the recent 5 (excluding the most recent rate).

        """
        #print('**** Interface.py STEP  1*****')
        rate = self.client.recv_plot('rate')


        rate_plot_data = rate['data']
        no_of_rate_plots = len(rate_plot_data)
        #print(no_of_rate_plots, '============')
        rate.data[0].visible = self.rates_live_rates_layer_select[0]
        rate['data'][0]['showlegend'] = True

        if no_of_rate_plots > 1:
            rate.data[1].visible = self.rates_live_rates_layer_select[1]
            rate['data'][1]['showlegend'] = True

            rate.data[2].visible = self.rates_live_rates_layer_select[2]
            rate['data'][2]['showlegend'] = True

            rate.data[3].visible = self.rates_live_rates_layer_select[3]
            rate['data'][3]['showlegend'] = True

            rate.data[4].visible = self.rates_live_rates_layer_select[4]
            rate['data'][4]['showlegend'] = True

            rate.data[5].visible = self.rates_live_rates_layer_select[5]
            rate['data'][5]['showlegend'] = True

            rate.data[6].visible = self.rates_live_rates_layer_select[6]
            rate['data'][6]['showlegend'] = True

            rate.data[7].visible = self.rates_live_rates_layer_select[7]
            rate['data'][7]['showlegend'] = True

            rate.data[8].visible = self.rates_live_rates_layer_select[8]
            rate['data'][8]['showlegend'] = True

            rate.data[9].visible = self.rates_live_rates_layer_select[9]
            rate['data'][9]['showlegend'] = True

        # Start with 1.
        avg_rate_0 = np.mean(rate_plot_data[0]['y'][-10:-1])
        avg_rate_txt_0 = f"{avg_rate_0:.4g}"

        max_rate_0 = np.max(rate_plot_data[0]['y'])
        max_rate_txt_0 = f"{max_rate_0:.4g}"
        #avg_rate_1 = np.mean(rate.data[1]['y'][-5:-1])
        #avg_rate_2 = np.mean(rate.data[2]['y'][-5:-1])

        livetime_plot = self.client.recv_plot('livetime')
        #livetime_plot.data[1].visible=False
        livetime_plot['data'][0]['showlegend']=True
       # livetime_plot['data'][1]['showlegend']=True

        #livetime = self.client.recv_val('livetime')
        livetime_txt = '100'

        # --- Snapshots 1-4 ---
        adc_snap_a = self.client.recv_plot('adc-snapshot-a')
        adc_snap_plot_data_a = adc_snap_a['data']
        no_of_snap_plots_a = len(adc_snap_plot_data_a)

        adc_snap_a.data[0].visible = self.adc_snap_layer_select_a[0]
        adc_snap_a['data'][0]['showlegend'] = True
        adc_snap_a.data[1].visible = self.adc_snap_layer_select_a[1]
        adc_snap_a['data'][1]['showlegend'] = True
        if no_of_snap_plots_a > 2:
            adc_snap_a.data[2].visible = self.adc_snap_layer_select_a[2]
            adc_snap_a['data'][2]['showlegend'] = True
            adc_snap_a.data[3].visible = self.adc_snap_layer_select_a[3]
            adc_snap_a['data'][3]['showlegend'] = True

            adc_snap_a.data[4].visible = self.adc_snap_layer_select_a[4]
            adc_snap_a['data'][4]['showlegend'] = True
            adc_snap_a.data[5].visible = self.adc_snap_layer_select_a[5]
            adc_snap_a['data'][5]['showlegend'] = True

            adc_snap_a.data[6].visible = self.adc_snap_layer_select_a[6]
            adc_snap_a['data'][6]['showlegend'] = True
            adc_snap_a.data[7].visible = self.adc_snap_layer_select_a[7]
            adc_snap_a['data'][7]['showlegend'] = True

            adc_snap_a.data[8].visible = self.adc_snap_layer_select_a[8]
            adc_snap_a['data'][8]['showlegend'] = True
            adc_snap_a.data[9].visible = self.adc_snap_layer_select_a[9]
            adc_snap_a['data'][9]['showlegend'] = True

            #adc_snap.data[10].visible = self.adc_snap_layer_select[10]
            #adc_snap['data'][10]['showlegend'] = True
            #adc_snap.data[11].visible = self.adc_snap_layer_select[11]
            #adc_snap['data'][11]['showlegend'] = True

        # --- Snapshots 5-9 ---
        adc_snap_b = self.client.recv_plot('adc-snapshot-b')
        adc_snap_plot_data_b = adc_snap_a['data']
        no_of_snap_plots_b = len(adc_snap_plot_data_b)

        adc_snap_b.data[0].visible = self.adc_snap_layer_select_b[0]
        adc_snap_b['data'][0]['showlegend'] = True
        adc_snap_b.data[1].visible = self.adc_snap_layer_select_b[1]
        adc_snap_b['data'][1]['showlegend'] = True
        if no_of_snap_plots_b > 2:
            adc_snap_b.data[2].visible = self.adc_snap_layer_select_b[2]
            adc_snap_b['data'][2]['showlegend'] = True
            adc_snap_b.data[3].visible = self.adc_snap_layer_select_b[3]
            adc_snap_b['data'][3]['showlegend'] = True

            adc_snap_b.data[4].visible = self.adc_snap_layer_select_b[4]
            adc_snap_b['data'][4]['showlegend'] = True
            adc_snap_b.data[5].visible = self.adc_snap_layer_select_b[5]
            adc_snap_b['data'][5]['showlegend'] = True

            adc_snap_b.data[6].visible = self.adc_snap_layer_select_b[6]
            adc_snap_b['data'][6]['showlegend'] = True
            adc_snap_b.data[7].visible = self.adc_snap_layer_select_b[7]
            adc_snap_b['data'][7]['showlegend'] = True

            adc_snap_b.data[8].visible = self.adc_snap_layer_select_b[8]
            adc_snap_b['data'][8]['showlegend'] = True
            adc_snap_b.data[9].visible = self.adc_snap_layer_select_b[9]
            adc_snap_b['data'][9]['showlegend'] = True

        histograms = make_plot_gen_str(self.histogram_plot_channels)
        histories = make_plot_gen_str(self.adc_history_plot_channels)

        event_count_msg = self.client.recv_val('n-packets')
        #print(f"---- event-count-msg : {event_count_msg}---")
        event_count_list = event_count_msg.split('c')

        layer_status_string = self.client.recv_val('layer-status-str')
        layer_status_str_arr = update_layer_status_class_string(layer_status_string)

        hist_bad_packet_count = self.client.recv_val('bad-hist-packet-counter')

        logger.debug("Pushing plot updates...")

        self.app.push_mods({
            'rate_timeseries':      {'figure': rate}, # updates rate_timeseries, figure with rate.
            'avg-rate-0':           {'children': avg_rate_txt_0},
            'max-rate-0':           {'children': max_rate_txt_0},
            'hist_bad_packet_count':{'children': hist_bad_packet_count},
            'snapshot-adc-a':       {'figure': adc_snap_a},
            'snapshot-adc-b':       {'figure': adc_snap_b},
            'histogram-adc':        {'figure': self.client.recv_plot(f'adc-hist:{histograms}')},
            'history-adc':          {'figure': self.client.recv_plot(f'history:{histories}')},
            'live-livetime-series': {'figure': livetime_plot},
            'l0-led': {'className': layer_status_str_arr[0]},
            'l1-led': {'className': layer_status_str_arr[1]},
            'l2-led': {'className': layer_status_str_arr[2]},
            'l3-led': {'className': layer_status_str_arr[3]},
            'l4-led': {'className': layer_status_str_arr[4]},
            'l5-led': {'className': layer_status_str_arr[5]},
            'l6-led': {'className': layer_status_str_arr[6]},
            'l7-led': {'className': layer_status_str_arr[7]},
            'l8-led': {'className': layer_status_str_arr[8]},
            'l9-led': {'className': layer_status_str_arr[9]},
            'event-count-0' : {'children': str(event_count_list[0])},
        })

        for i in range(no_of_rate_plots):
            if i !=0:
                avg_rate_i = np.mean(rate_plot_data[i]['y'][-5:-1])
                avg_rate_txt_i = f"{avg_rate_i:.4g}"
                rate_avg_var = 'avg-rate-'+str(i)

                max_rate_i = np.max(rate_plot_data[i]['y'])
                max_rate_txt_i = f"{max_rate_i:.4g}"
                rate_max_var = 'max-rate-'+str(i)

                event_count_txt_i = str(event_count_list[i])
                event_count_var = 'event-count-'+str(i)

                self.app.push_mods({
                    rate_avg_var:           {'children': avg_rate_txt_i},
                    rate_max_var:           {'children': max_rate_txt_i},
                    event_count_var:        {'children': event_count_txt_i}
                })

        logger.debug("Done pushing...")

        self.timer = Timer(0.7, self.timer_callback)
        self.timer.start() 

