#!/usr/bin/env python3
"""A test emitter to replicate a layer using a data file.

Modified it from 2 to 6
"""

import argparse
from datetime import timedelta
import time

from timeloop import Timeloop
import zmq

import silayer

tl = Timeloop()

# List of all 10 local ports. (9998 is the port address for the layer)
L0_PORT = "9998"
L1_PORT = "9980"
L2_PORT = "9981"
L3_PORT = "9982"
L4_PORT = "9983"
L5_PORT = "9984"
L6_PORT = "9985"
L7_PORT = "9986"
L8_PORT = "9987"
L9_PORT = "9988"

class MultiPacketEmit:

    def __init__(self, data_path, packet_rate):
        """Initialize multi-packet emitter.

        Please call tl.start soon after initializing for the `tstart` to matter.

        Parameters
        ----------
        data_path: str
            Path to the raw binary file
        packet_rate: int or float
            Rate, in Hz, at which to emit packets.
        """
        self.iterator = silayer.raw2hdf.lame_byte_iterator(data_path)
        self.rate = packet_rate

        self.context = zmq.Context()

        self.socket1 = self.context.socket(zmq.PUB)
        self.socket1.setsockopt(zmq.LINGER, 1)
        self.socket1.bind(f"tcp://*:{L0_PORT}")
        print(f"Binded to socket.{L0_PORT} running")

        self.socket2 = self.context.socket(zmq.PUB)
        self.socket2.setsockopt(zmq.LINGER, 1)
        self.socket2.bind(f"tcp://*:{L1_PORT}")
        print(f"Binded to socket.{L1_PORT} running")

        self.socket3 = self.context.socket(zmq.PUB)
        self.socket3.setsockopt(zmq.LINGER, 1)
        self.socket3.bind(f"tcp://*:{L2_PORT}")
        print(f"Binded to socket.{L2_PORT} running")

        self.socket4 = self.context.socket(zmq.PUB)
        self.socket4.setsockopt(zmq.LINGER, 1)
        self.socket4.bind(f"tcp://*:{L3_PORT}")
        print(f"Binded to socket.{L3_PORT} running")

        self.socket5 = self.context.socket(zmq.PUB)
        self.socket5.setsockopt(zmq.LINGER, 1)
        self.socket5.bind(f"tcp://*:{L4_PORT}")
        print(f"Binded to socket.{L4_PORT} running")

        self.socket6 = self.context.socket(zmq.PUB)
        self.socket6.setsockopt(zmq.LINGER, 1)
        self.socket6.bind(f"tcp://*:{L5_PORT}")
        print(f"Binded to socket.{L5_PORT} running")

        self.socket7 = self.context.socket(zmq.PUB)
        self.socket7.setsockopt(zmq.LINGER, 1)
        self.socket7.bind(f"tcp://*:{L6_PORT}")
        print(f"Binded to socket.{L6_PORT} running")

        self.socket8 = self.context.socket(zmq.PUB)
        self.socket8.setsockopt(zmq.LINGER, 1)
        self.socket8.bind(f"tcp://*:{L7_PORT}")
        print(f"Binded to socket.{L7_PORT} running")

        self.socket9 = self.context.socket(zmq.PUB)
        self.socket9.setsockopt(zmq.LINGER, 1)
        self.socket9.bind(f"tcp://*:{L8_PORT}")
        print(f"Binded to socket.{L8_PORT} running")

        self.socket10 = self.context.socket(zmq.PUB)
        self.socket10.setsockopt(zmq.LINGER, 1)
        self.socket10.bind(f"tcp://*:{L9_PORT}")
        print(f"Binded to socket.{L9_PORT} running")

        self.faux_data_iterator = silayer.raw2hdf.lame_byte_iterator(data_path)


        self.tstart = time.time()
        self.t0 = time.time()
        self.i = 0
        self.big_tlast = self.tstart
        self.refresh_interval = packet_rate

        # register time loop job
        interval = timedelta(seconds=1./self.rate)
        tl.job(interval=interval)(self.send_packets)

    def send_packets(self):
        tnow = time.time()

        test_data1 = next(self.faux_data_iterator)
        test_data2 = next(self.faux_data_iterator)
        test_data3 = next(self.faux_data_iterator)
        test_data4 = next(self.faux_data_iterator)
        test_data5 = next(self.faux_data_iterator)
        test_data6 = next(self.faux_data_iterator)
        test_data7 = next(self.faux_data_iterator)
        test_data8 = next(self.faux_data_iterator)
        test_data9 = next(self.faux_data_iterator)
        test_data10 = next(self.faux_data_iterator)

        iterator_dt = time.time() - tnow

        socket_t0 = time.time()

        self.socket1.send(test_data1)  # , flags=zmq.NOBLOCK, copy=False)
        self.socket2.send(test_data2)
        self.socket3.send(test_data3)
        self.socket4.send(test_data4)
        self.socket5.send(test_data5)
        self.socket6.send(test_data6)
        self.socket7.send(test_data7)
        self.socket8.send(test_data8)
        self.socket9.send(test_data9)
        self.socket10.send(test_data10)


        socket_dt = time.time() - socket_t0

        # How frequently is this supposed to print???
        if self.i % self.refresh_interval == 0 and self.i > 0:
            deltaT = tnow - self.t0
            rate = 1. / deltaT
            print(
                f"{self.i} - DT: {tnow - self.t0:8.3g}",
                f"-- bigDT {tnow - self.big_tlast:8.3g}",
                f"-- socketDT: {socket_dt:8.3g}",
                f"-- iterDT: {iterator_dt:8.3g}",
                f"-- Rate: {rate:8.3g} Hz",
            )
            self.big_tlast = tnow

        self.t0 = tnow
        self.i += 1


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("packet_rate", help="Packet rate in Hz.", type=float)
    parser.add_argument("path", help="Path to data file to send.")
    #parser.add_argument("-N", type=int, default=10,
                        #help="No of Layers to simulate for testbench. Default is 10. ")

    args = parser.parse_args()

    emitter = MultiPacketEmit(args.path, args.packet_rate)

    tl.start(block=True)  # Run timeloop in main thread. This should cleanup on exit
