""" @@@Callback module.

    This module contains additional callbacks to the dash application.
    Sphinx has some issues with documenting decorated functions so these might not show
    properly in the documentation.

    current callbacks:
        | update_click_output
        | reset_history
        | reset_histogram
        | asic_config_table
        | fee_interact

    Notes
    -----
    Currently it assumes default configs when importing... using silayer.cfg_reg

"""

# ComPair tracker quicklook Dash module.
# Author: Sean Griffin
#
#
# Register Dash callbacks to the app. 
# July 12, 2021     Sambid K. Wasti
#                   Added extensive comments and doc-strings.


import dash_devices
import time
from dash_devices.dependencies import Input, Output, State
from dash_devices.exceptions import PreventUpdate
import dash_html_components as html
import logging

import layout

import subprocess

import silayer
from silayer.cfg_reg import VataCfg
from silayer.cfg_reg import neg_default_vcfg, pos_default_vcfg

logger = logging.getLogger(__name__)

def register_callbacks(iface):
    """ Additional callbacks. Sadly sphinx is avoiding it atm.

    Parameters
    ----------
    iface : Interface
        This is called at interface initialization so this is all the self component of interface.

    """
    # ======= Callbacks for modal popup =======
    @iface.app.callback(
        Output("markdown", "style"),
        [Input("learn-more-button", "n_clicks"), Input("markdown_close", "n_clicks")],
    )
    def update_click_output(button_click, close_click):
        """

        Parameters
        ----------
        button_click :
        close_click :

        Returns
        -------

        """
        ctx = dash_devices.callback_context

        if ctx.triggered:
            prop_id = ctx.triggered[0]["prop_id"].split(".")[0]
            if prop_id == "learn-more-button":
                return {"display": "block"}

        return {"display": "none"}



    # Plot resetting history callbacks:
    @iface.app.callback_shared(None, [Input('history-reset-data', 'n_clicks')])
    def reset_history(n_clicks):
        response = iface.client.send_recv("reset_history")
        logger.debug(f"{response}")

    # Layer status reset button callbacks:
    @iface.app.callback(None, [Input('layer-status-reset', 'n_clicks')])
    def reset_layer_status(n_clicks):
        response = iface.client.send_recv("reset_lstatus")
        logger.debug(f"{response}")


    # Plot resetting Snapshot (equivalent to history) callbacks:
    @iface.app.callback(None, [Input('adc-snap-reset', 'n_clicks')])
    def reset_snapshot(n_clicks):
        response = iface.client.send_recv("reset_history")
        logger.debug(f"{response}")

    # Histogram Resetting callbacks:
    @iface.app.callback_shared(None, [Input('histogram-reset-data', 'n_clicks')])
    def reset_histogram(n_clicks):
        response = iface.client.send_recv("reset_histogram")            
        logger.debug(f"{response}")



            # ASIC query stuff. (This is obsolete and will be removed)
    @iface.app.callback_shared(
        [
            Output('asic-cfg-table', 'children')
        ], 
        [
            Input('query-asic-cfg-button', 'n_clicks')
        ],
        [
            State('query-asic-vata-in', 'value')
        ])
    def asic_config_table(n_clicks, vata):

        if vata is None:
            raise PreventUpdate

        vata = int(vata)
        table_header = html.Thead(html.Tr([html.Th("Field"), html.Th("Value")]))
        table_body = []

        useful_fields = ["vthr", "bias_dac_iramp", "nside"]

        for field in useful_fields:
            ### SG: Lucas, you'll need to do a query to the server here; 
            ### This is just dummy text.
            if vata < 6:
                cfg = neg_default_vcfg
            else:
                cfg = pos_default_vcfg

            cfg_val = cfg[field]

            if field == "nside": 
                if cfg_val == 1:
                    cfg_val = "NEG"
                else:
                    cfg_val = "POS"

                field = "polarity"

                
            row = html.Tr([html.Td(field), html.Td(cfg_val)])

            table_body += [row]

        return [table_body]
        #row1 = html.Tr([html.Td("Arthur"), html.Td("Dent")])




    # FEE control
    #This callback handles the interactions for fee_enable/disable.
    @iface.app.callback(
        [
            Output("fee-status-field", "children")
        ],
        [
            Input("fee-enable-button", "n_clicks"),
            Input("fee-disable-button", "n_clicks"),
            Input("fee-query-button", "n_clicks"),            
        ],
        
    )
    def fee_interact(enable_clicks, disable_clicks, query_clicks):
        
        host_ip = iface.client.hosts

        return_str = ""
        if not iface.ctx.triggered:
            return_str = "Unknown!"
        else:
            triggered = iface.ctx.triggered[0]['prop_id'].split('.')[0] 
            if triggered == 'fee-enable-button':
                command_str = f"ssh root@{host_ip} \"echo 1 >> /sys/class/gpio/gpio906/value\" "
                subprocess.Popen(f"{command_str}", shell=True, stdout=subprocess.PIPE).stdout

            elif triggered == 'fee-disable-button':
                command_str = f"ssh root@{host_ip} \"echo 0 >> /sys/class/gpio/gpio906/value\" "
                subprocess.Popen(f"{command_str}", shell=True, stdout=subprocess.PIPE).stdout

            elif triggered == 'fee-query-button':
                pass 
                
            command_str = f"ssh root@{host_ip} \"cat /sys/class/gpio/gpio906/value\" "
            get_fee_status =  subprocess.Popen(f"{command_str}", shell=True, stdout=subprocess.PIPE).stdout
            fee_status =  get_fee_status.read().decode('ascii').rstrip('\n')
            
            
            if fee_status == '1':
                return_str = "ENABLED"
            else: 
                return_str = "DISABLED"


        return [return_str]                  



