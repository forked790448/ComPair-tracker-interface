#!/usr/bin/env python

"""
Main executable for ComPair Tracker Interface application.

#>>> python run.py
#>>> python run.py --host

"""
# July 9, 2021      Sambid K. Wasti


from client import Client
from interface import Interface
import dash_devices
import dash_bootstrap_components as dbc
import logging
import os
import csv


interface_version = "2.0"

# Current Number of Max Active layers.
NO_OF_CUR_LAYERS = 10

#All 10 Layer Server's Host Addresses
L0_HOST = "10.10.0.20"
L1_HOST = "10.10.0.21"
L2_HOST = "10.10.0.22"
L3_HOST = "10.10.0.23"
L4_HOST = "10.10.0.24"
L5_HOST = "10.10.0.25"
L6_HOST = "10.10.0.26"
L7_HOST = "10.10.0.27"
L8_HOST = "10.10.0.28"
L9_HOST = "10.10.0.29"


LAYER_HOST_ARRAY = [L0_HOST,L1_HOST,L2_HOST,L3_HOST,L4_HOST,L5_HOST,L6_HOST,L7_HOST,L8_HOST,L9_HOST]

#Array for Local Hosts.

LOCAL_HOST_ARRAY = ["localhost","localhost","localhost","localhost","localhost",
                    "localhost","localhost","localhost","localhost","localhost"]

# List of all 10 local ports. (9998 is the port address for the layer)
L0_PORT = "9998"
L1_PORT = "9980"
L2_PORT = "9981"
L3_PORT = "9982"
L4_PORT = "9983"
L5_PORT = "9984"
L6_PORT = "9985"
L7_PORT = "9986"
L8_PORT = "9987"
L9_PORT = "9988"

#Themes used, COSMO, FLATLY, MATERIA, BOOTSTRAP
app = dash_devices.Dash(
    __name__,
    external_stylesheets=[dbc.themes.BOOTSTRAP],
    meta_tags=[{"name": "viewport", "content": "width=device-width, initial-scale=1"}],
)

#server = app.server
app.config["suppress_callback_exceptions"] = True

#Local Ports Array
LOCAL_PORTS = [L0_PORT,L1_PORT,L2_PORT,L3_PORT,L4_PORT,L5_PORT,L6_PORT,L7_PORT,L8_PORT,L9_PORT]
#Layer Ports Array.
LAYER_PORTS = [L0_PORT,L0_PORT,L0_PORT,L0_PORT,L0_PORT,L0_PORT,L0_PORT,L0_PORT,L0_PORT,L0_PORT]

# NOW we redefine the array for no. of layers.
LAYER_HOST_ARRAY = LAYER_HOST_ARRAY[:NO_OF_CUR_LAYERS]
LAYER_PORTS = LAYER_PORTS[:NO_OF_CUR_LAYERS]
LOCAL_HOST_ARRAY = LOCAL_HOST_ARRAY[:NO_OF_CUR_LAYERS]
LOCAL_PORTS = LOCAL_PORTS[:NO_OF_CUR_LAYERS]

if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description="Run the data receiver")
    parser.add_argument("--host", type=str, default=None, help="Silicon layer host IP")
    #-- Two Host
    parser.add_argument("--twohost", action ='store_true', help=" Run for two hosts" )
    parser.add_argument("-ip",type=str, nargs=2, default=['10.10.0.21','10.10.0.23'],
                        help="ip address of both layers separated by space" )
    #-- Test Bench
    parser.add_argument("--testbench", action='store_true', help=" Running for testing.")
    parser.add_argument("-N", type=int, default=NO_OF_CUR_LAYERS,
                        help="No of Layers to simulate for testbench. Default is 10. ")
    parser.add_argument("--log", type=int, default=0,
                        help="logging level for debugging. 0= no logging, 1=debug and 2=info")

    args = parser.parse_args()

    # LOGGING
    log_level = 0
    logging.basicConfig(filename='interface.log',level=log_level)
    logger = logging.getLogger(__name__)
    #ch = logging.StreamHandler()


    if args.log:
        if args.log == 1:
            log_level = 10 # Debug
        elif args.log == 2:
            log_level = 20 # Info
        else:
            log_level = 0 # NotSet

    print(log_level, "LOG LEVEL")
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    #ch.setFormatter(formatter)
    #logger.addHandler(ch)

    if not args.testbench:
        if not args.twohost:
            if not args.host:
                logger.debug("Connecting to all Layers 1-9.")
                iface = Interface(app, Client(hosts=LAYER_HOST_ARRAY, data_ports=LAYER_PORTS, log_level = log_level),
                                  interface_version=interface_version,log_level = log_level)
                response = iface.client.send_recv("connect")
                logger.info(f"Connected client to server; receieved response {response}")
            else:
                logger.debug("Connecting to single layer")

                lay_host_arr = [args.host]
                lay_port_arr = [L0_PORT]
                iface = Interface(app, Client(hosts=lay_host_arr, data_ports=lay_port_arr, log_level = log_level),
                                  interface_version=interface_version,log_level = log_level)
                response = iface.client.send_recv("connect")  # this is running at
                logger.info(f"Connected client to server; receieved response {response}")
        else:
            logger.debug("Connecting to 2 layers")
            print("Connecting to these hosts: ")
            lay_host_arr_2 = args.ip
            lay_port_arr_2 = [L0_PORT, L0_PORT]
            print(lay_host_arr_2)
            iface = Interface(app, Client(hosts=lay_host_arr_2, data_ports=lay_port_arr_2, log_level = log_level),
                                  interface_version=interface_version,log_level = log_level)
            response = iface.client.send_recv("connect")
            logger.info(f"Connected client to server; receieved response {response}")
    else:
        logger.debug("Creating layout-only testbench.")
        loc_host_arr = LOCAL_HOST_ARRAY[:args.N]
        loc_port_arr = LOCAL_PORTS[:args.N]

        iface = Interface(app, Client(hosts=loc_host_arr, data_ports=loc_port_arr, log_level = log_level),
                          interface_version = interface_version, log_level = log_level  )
        response = iface.client.send_recv("connect") # this is running at
        logger.info(f"Connected client to server; receieved response {response}")


    app.run_server(debug=True, host='0.0.0.0', port=5000)
