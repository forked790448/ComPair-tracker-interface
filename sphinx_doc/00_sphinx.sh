#!/bin/bash
sphinx-apidoc -fFM -o . ../tracker_interface
sphinx-apidoc -fM -o . ../tracker_interface
cp -r _temp_files/* .
mv ./index.txt ./index.rst
make clean
make html
